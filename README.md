Diploma Backend
==========

A Symfony project created on February 4, 2017, 4:39 pm.
--------------
- php5.6 bin/console generate:bundle
- php5.6 bin/console server:start 
- http://localhost:8000 
- php5.6 bin/console doctrine:database:create
- php5.6 bin/console doctrine:schema:update --force
- php5.6 bin/console doctrine:generate:entities Maksi
- php5.6 app/console generate:doctrine:crud

composer require friendsofsymfony/rest-bundle
composer require jms/serializer-bundle
composer require nelmio/cors-bundle

new FOS\RestBundle\FOSRestBundle(),
new JMS\SerializerBundle\JMSSerializerBundle(),
new Nelmio\CorsBundle\NelmioCorsBundle(),


http://stackoverflow.com/questions/35346342/which-type-of-folder-structure-should-be-used-with-angular-2