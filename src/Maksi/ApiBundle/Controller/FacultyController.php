<?php

namespace Maksi\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

use Maksi\BusinessBundle\Entity\Faculty;



class FacultyController extends FOSRestController{


    /**
     * @Rest\Get("/faculty")
     */
    public function getAction()
    {
        $restResult = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Faculty')->findAll();
        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return array_reverse($restResult);
    }

    /**
     * @Rest\Post("/faculty")
     */
    public function postAction(Request $request)
    {

        $data = new Faculty();
        $name = $request->get('_name');
        $shortName = $request->get('_shortName');

        if(empty($name) || empty($shortName))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setName($name);
        $data->setShortName($shortName);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/faculty/{id}")
     */
    public function putAction(Request $request, $id)
    {

        if($id) {
            $data = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Faculty')->find($id);
            $name = $request->get('_name');
            $shortName = $request->get('_shortName');
            $data->setName($name);
            $data->setShortName($shortName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            return new View($data, Response::HTTP_OK);
        }else{
            $this->postAction($request);
        }
    }

    /**
     * @Rest\Delete("/faculty/{id}")
     */
    public function deleteAction(Request $request, $id)
    {
        $data = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Faculty')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/faculty/{id}")
     */
    public function idAction($id)
    {
        $restResult = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Faculty')->find($id);
        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return $restResult;
    }

}

