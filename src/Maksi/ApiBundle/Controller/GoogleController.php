<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16.04.17
 * Time: 19:14
 */


namespace Maksi\ApiBundle\Controller;

use Maksi\BusinessBundle\Entity\FacultyPeriod;
use Maksi\BusinessBundle\Entity\Groups;
use Maksi\BusinessBundle\Entity\Student;
use Maksi\BusinessBundle\Entity\StudentPeriodGroup;
use Maksi\BusinessBundle\Repository\FacultyPeriodRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Maksi\BusinessBundle\Entity\Period;

class GoogleController extends FOSRestController
{

    /**
     * Create Google Spread Sheet for DB
     *
     * @param $periodId
     * @param $facultyId
     * @return View
     *
     * @Rest\Post("/google/period/{periodId}/faculty/{facultyId}/db/")
     */
    public function createGoogleSheetDb($periodId, $facultyId)
    {
        /**@var $period Period */
        $period = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Period')->find($periodId);
        $faculty = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Faculty')->find($facultyId);

        $sheetData = array(
            'name' => $period->getName() . ' | ' . $faculty->getName() . ' | DB',
            'email' => 't.blindaruK@gmail.com'
        );

        /**@var $groupSheets \Google_Service_Sheets_Spreadsheet */
        $groupSheets = $this->container->get('google.client.sheets')->createSheets($sheetData);
        $groupSheetsId = $groupSheets->getSpreadsheetId();

        $facultyPeriod = new FacultyPeriod();
        $facultyPeriod->setFaculty($faculty);
        $facultyPeriod->setPeriod($period);
        $facultyPeriod->setDbSheetsId($groupSheetsId);

        foreach ($groupSheets->getSheets() as $sheet) {
            $properties = $sheet->getProperties();
            switch ($properties->getTitle()) {
                case 'Групи':
                    $facultyPeriod->setGroupsSheetId($properties->getSheetId());
                    break;
                case 'Студенти':
                    $facultyPeriod->setStudentsSheetId($properties->getSheetId());
                    break;
                case 'Предмети':
                    $facultyPeriod->setSubjectsSheetId($properties->getSheetId());
                    break;
            }
        }


        $em = $this->getDoctrine()->getManager();
        $em->persist($facultyPeriod);
        $em->flush();

        return new View(array('sheetId' => $groupSheetsId), Response::HTTP_OK);
    }

    /**
     * Create Google Spread Sheet for DB
     *
     * @param $periodId
     * @param $facultyId
     * @return View
     *
     * @Rest\Post("/google/period/{periodId}/faculty/{facultyId}/data/")
     */
    public function createData($periodId, $facultyId)
    {
        /**@var $facultyPeriodRepository FacultyPeriodRepository */
        $facultyPeriodRepository = $this->getDoctrine()->getRepository('MaksiBusinessBundle:FacultyPeriod');

        /**@var $facultyPeriod FacultyPeriod */
        $facultyPeriod = $facultyPeriodRepository->findOneBy(
            array(
                'period' => $periodId,
                'faculty' => $facultyId
            )
        );

        $data = null;
        if ($facultyPeriod) {
            $sheetId = $facultyPeriod->getDbSheetsId();
            if (isset($sheetId)) {
                $sheetData = array(
                    'spreadSheetId' => $sheetId,
                    'email' => 't.blindaruk@gmail.com'
                );
                $data = $this->container->get('google.client.sheets')->createRatingSheets($sheetData);
                if ($data) {
                    $this->container->get('google.client.sheets')->changePermissionToRead($sheetData);
                }
            }
            $data = empty($data) ? null : $data;
            $facultyPeriod->setRatingSubjectLists(json_encode($data));
            $em = $this->getDoctrine()->getManager();
            $em->persist($facultyPeriod);
            $em->flush();
        }

        return new View($data, Response::HTTP_OK);
    }

    /**
     * Create Google Spread Sheet for DB
     *
     * @param $periodId
     * @param $facultyId
     * @return View
     *
     * @Rest\Post("/google/period/{periodId}/faculty/{facultyId}/result/")
     */
    public function createResultData($periodId, $facultyId)
    {

        /**@var $facultyPeriodRepository FacultyPeriodRepository */
        $facultyPeriodRepository = $this->getDoctrine()->getRepository('MaksiBusinessBundle:FacultyPeriod');

        /**@var $facultyPeriod FacultyPeriod */
        $facultyPeriod = $facultyPeriodRepository->findOneBy(
            array(
                'period' => $periodId,
                'faculty' => $facultyId
            )
        );

        $sheetId = $facultyPeriod->getDbSheetsId();
        if (isset($sheetId)) {

            $data = $this->container->get('google.client.sheets')->createResultRatingSheets(json_decode($facultyPeriod->getRatingSubjectLists(), true));
        }
        $data = empty($data) ? null : $data;
        $facultyPeriod->setResultRatingSubjectLists(json_encode($data));
        $em = $this->getDoctrine()->getManager();
        $em->persist($facultyPeriod);
        $em->flush();

        return new View($data, Response::HTTP_OK);
    }

    /**
     * Create Google Spread Sheet for DB
     *
     * @param $periodId
     * @param $facultyId
     * @return View
     *
     * @Rest\Post("/google/period/{periodId}/faculty/{facultyId}/grants/")
     */
    public function createGrantsList($periodId, $facultyId)
    {

        /**@var $facultyPeriodRepository FacultyPeriodRepository */
        $facultyPeriodRepository = $this->getDoctrine()->getRepository('MaksiBusinessBundle:FacultyPeriod');

        /**@var $facultyPeriod FacultyPeriod */
        $facultyPeriod = $facultyPeriodRepository->findOneBy(
            array(
                'period' => $periodId,
                'faculty' => $facultyId
            )
        );
        $data = null;
        if ($facultyPeriod) {
            $sheetId = $facultyPeriod->getDbSheetsId();
            if (isset($sheetId) && $facultyPeriod->getResultRatingSubjectLists()) {

                $data = $this->container->get('google.client.sheets')->createGrantList($sheetId, json_decode($facultyPeriod->getResultRatingSubjectLists(), true));
            }

            $data = empty($data) ? null : $data;

            $groups = [];
            if (isset($data['students'])) {
                foreach ($data['students'] as $student) {
                    $groupName = $student['1'];
                    if (!isset($groups[$groupName])) {
                        $groups[$groupName] = $groupName;
                        $studentPeriodGroups = $this->getDoctrine()->getRepository('MaksiBusinessBundle:StudentPeriodGroup')
                            ->findBy(
                                array(
                                    'group' => $this->getDoctrine()->getRepository('MaksiBusinessBundle:Groups')->findBy(array('name' => $groupName)),
                                    'period'=>$this->getDoctrine()->getRepository('MaksiBusinessBundle:Period')->find($periodId)
                                )
                            );

                        /**@var $studentPeriodGroup StudentPeriodGroup */
                        foreach ($studentPeriodGroups as $studentPeriodGroup) {
                            $em = $this->getDoctrine()->getEntityManager();
                            $em->remove($studentPeriodGroup->getStudent());
                            $em->flush();
                        }
                    }
                }

                foreach ($data['students'] as $student) {
                    $groupsRepository = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Groups');
                    $group = $groupsRepository->findOneBy(array('name' => $student['1']));
                    if (!$group) {
                        $group = new Groups();
                        $group->setName($student['1']);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($group);
                        $em->flush();
                    }

                    $studentEntity = new Student();
                    $studentEntity->setName($student[2]);
                    $studentEntity->setRating($student[4]);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($studentEntity);
                    $em->flush();

                    $studentPeriodGroup = new StudentPeriodGroup();
                    $studentPeriodGroup->setGroup($group);
                    $studentPeriodGroup->setStudent($studentEntity);
                    $studentPeriodGroup->setPeriod($this->getDoctrine()->getRepository('MaksiBusinessBundle:Period')->find($periodId));


                    $em = $this->getDoctrine()->getManager();
                    $em->persist($studentPeriodGroup);
                    $em->flush();
                }
            }

            $facultyPeriod->setScholarship($data['sheetId']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($facultyPeriod);
            $em->flush();
        }

        return new View(array('sheetId' => $data['sheetId']), Response::HTTP_OK);
    }
}