<?php

namespace Maksi\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Maksi\BusinessBundle\Entity\Groups;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class GroupController extends FOSRestController
{


    /**
     * @Rest\Get("/group")
     */
    public function getAction()
    {
        $restResult = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Groups')->findAll();
        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return array_reverse($restResult);
    }

    /**
     * @param Request $request
     * @return View
     *
     * @Rest\Post("/group")
     */
    public function postAction(Request $request)
    {
        $data = new Groups();
        $data->setName($request->get('name'));
        $data->setSpecialty($this->getDoctrine()->getRepository('MaksiBusinessBundle:Specialty')->find($request->get('specialtyId')));
        $data->setStartYear($request->get('startYear'));
        $data->setType($request->get('groupType'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }

    /**`
     * @Rest\Put("/group/{id}")
     */
    public function putAction(Request $request, $id)
    {

        if ($id) {
            $data = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Groups')->find($id);

            $data->setName($request->get('name'));
            $data->setSpecialty($this->getDoctrine()->getRepository('MaksiBusinessBundle:Specialty')->find($request->get('specialtyId')));

            $startYear = $request->get('startYear');
            $startYear = empty($startYear) ? null : $startYear;
            $data->setStartYear($startYear);

            $groupType = $request->get('groupType');
            $groupType = empty($groupType) ? null : $groupType;
            $data->setType($groupType);

            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            return new View($data, Response::HTTP_OK);
        } else {
            $this->postAction($request);
        }
    }

    /**
     * @Rest\Delete("/group/{id}")
     */
    public function deleteAction(Request $request, $id)
    {
        $data = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Groups')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }
}

