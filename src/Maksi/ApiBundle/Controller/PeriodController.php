<?php

namespace Maksi\ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

use Maksi\BusinessBundle\Entity\Period;

class PeriodController extends FOSRestController
{
    /**
     * @Rest\Get("/period")
     */
    public function getAction()
    {
        $restResult = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Period')->findAll();
        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return array_reverse($restResult);
    }

    /**
     * @Rest\Post("/period")
     */
    public function postAction(Request $request)
    {

        $data = new Period();
        $name = $request->get('name');

        if(empty($name))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setName($name);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/period/{id}")
     */
    public function idAction($id)
    {
        $restResult = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Period')->find($id);
        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return $restResult;
    }

}
