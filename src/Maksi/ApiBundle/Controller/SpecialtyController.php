<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 01.04.17
 * Time: 22:56
 */

namespace Maksi\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

use Maksi\BusinessBundle\Entity\Specialty;

class SpecialtyController extends FOSRestController{

    /**
     * @Rest\Get("/specialty")
     */
    public function getAction()
    {
        $restResult = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Specialty')->findAll();
        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return array_reverse($restResult);
    }

    /**
     * @Rest\Post("/specialty")
     */
    public function postAction(Request $request)
    {
        $data = new Specialty();
        $name = $request->get('name');
        $facultyId = $request->get('facultyId');

        if(empty($name) || empty($facultyId))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setName($name);
        $data->setFaculty($this->getDoctrine()->getRepository('MaksiBusinessBundle:Faculty')->find($facultyId));
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/specialty/{id}")
     */
    public function putAction(Request $request, $id)
    {
        $data = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Specialty')->find($id);
        $name = $request->get('name');
        $facultyId = $request->get('facultyId');

        if(empty($name) || empty($facultyId))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setName($name);
        $data->setFaculty($this->getDoctrine()->getRepository('MaksiBusinessBundle:Faculty')->find($facultyId));
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/specialty/{id}")
     */
    public function deleteAction(Request $request, $id)
    {
        $data = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Specialty')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }
}