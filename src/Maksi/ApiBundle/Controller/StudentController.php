<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 01.06.17
 * Time: 20:15
 */

namespace Maksi\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Maksi\BusinessBundle\Entity\Groups;
use Maksi\BusinessBundle\Entity\StudentPeriodGroup;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

use Maksi\BusinessBundle\Entity\Student;
use Maksi\BusinessBundle\Entity\Period;


class StudentController extends FOSRestController
{

    /**
     * @Rest\Get("/student")
     */
    public function getAction()
    {
        $restResult = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Student')->findAll();
        if ($restResult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return array_reverse($restResult);
    }

    /**
     * @Rest\Post("/student")
     */
    public function postAction(Request $request)
    {
        $name = $request->get('name');
        $periodId = $request->get('periodId');
        $groupId = $request->get('groupId');
        $parentId = $request->get('parentId');
        $rating = $request->get('rating');
        $img = $request->get('img');

        if(empty($name))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $student = new Student();
        $student->setName($name);
        $student->setRating($rating);
        $student->setSrc($img);
        $student->setIsRoot(empty($request->get(('isRoot'))) ? false : true);
        if($parentId) {
            $student->setParent($this->getDoctrine()->getRepository('MaksiBusinessBundle:Student')->find($parentId));
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($student);
        $em->flush();

        if($groupId && $periodId) {
            $studentPeriodGroup = new StudentPeriodGroup();
            $studentPeriodGroup->setStudent($student);
            $studentPeriodGroup->setPeriod($this->getDoctrine()->getRepository('MaksiBusinessBundle:Period')->find($periodId));
            $studentPeriodGroup->setGroup($this->getDoctrine()->getRepository('MaksiBusinessBundle:Groups')->find($groupId));

            $em = $this->getDoctrine()->getManager();
            $em->persist($studentPeriodGroup);
            $em->flush();
        }
        $em->clear();

        return new View($this->getDoctrine()->getRepository('MaksiBusinessBundle:Student')->find($student->getId()), Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/student/{id}")
     */
    public function putAction(Request $request, $id)
    {
        if($id) {
            /**@var $student Student*/
            $student = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Student')->find($id);
            $student->setName($request->get('name'));
            $student->setSrc($request->get('img'));
            $parent = $request->get('parentId');
            if($parent){
                $student->setParent($this->getDoctrine()->getRepository('MaksiBusinessBundle:Student')->find($parent));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();

            return new View($student, Response::HTTP_OK);
        }else{
            $this->postAction($request);
        }
    }

    /**
     * @Rest\Delete("/student/{id}")
     */
    public function deleteAction(Request $request, $id)
    {
        $data = $this->getDoctrine()->getRepository('MaksiBusinessBundle:Student')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($data);
        $em->flush();
        return new View($data, Response::HTTP_OK);
    }
}