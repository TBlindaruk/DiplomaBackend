<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 04.02.17
 * Time: 17:46
 */

namespace Maksi\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="Maksi\BusinessBundle\Repository\FacultyRepository")
 * @ORM\Table(name="faculty")
 */
class Faculty
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $shortName;

    /**
     * @ORM\OneToMany(targetEntity="Specialty", mappedBy="faculty")
     */
    private $specialties;

    /**
     * @ORM\OneToMany(targetEntity="FacultyPeriod", mappedBy="faculty")
     */
    private $facultyPeriod;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->specialties = new \Doctrine\Common\Collections\ArrayCollection();
        $this->facultyPeriod = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Faculty
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return Faculty
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Add specialty
     *
     * @param \Maksi\BusinessBundle\Entity\Specialty $specialty
     *
     * @return Faculty
     */
    public function addSpecialty(\Maksi\BusinessBundle\Entity\Specialty $specialty)
    {
        $this->specialties[] = $specialty;

        return $this;
    }

    /**
     * Remove specialty
     *
     * @param \Maksi\BusinessBundle\Entity\Specialty $specialty
     */
    public function removeSpecialty(\Maksi\BusinessBundle\Entity\Specialty $specialty)
    {
        $this->specialties->removeElement($specialty);
    }

    /**
     * Get specialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialties()
    {
        return $this->specialties;
    }

    /**
     * Add facultyPeriod
     *
     * @param \Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod
     *
     * @return Faculty
     */
    public function addFacultyPeriod(\Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod)
    {
        $this->facultyPeriod[] = $facultyPeriod;

        return $this;
    }

    /**
     * Remove facultyPeriod
     *
     * @param \Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod
     */
    public function removeFacultyPeriod(\Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod)
    {
        $this->facultyPeriod->removeElement($facultyPeriod);
    }

    /**
     * Get facultyPeriod
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFacultyPeriod()
    {
        return $this->facultyPeriod;
    }
}
