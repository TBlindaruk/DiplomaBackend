<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 22.04.17
 * Time: 16:02
 */

namespace Maksi\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="Maksi\BusinessBundle\Repository\FacultyPeriodRepository")
 * @ORM\Table(name="faculty_period")
 */
class FacultyPeriod{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Faculty", inversedBy="facultyPeriod")
     * @ORM\JoinColumn(name="faculty_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $faculty;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Period", inversedBy="facultyPeriod")
     * @ORM\JoinColumn(name="period_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $period;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dbSheetsId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $groupsSheetId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $subjectsSheetId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $studentsSheetId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ratingSubjectLists;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resultRatingSubjectLists;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $scholarship;

    /**
     * @return mixed
     */
    public function getScholarship()
    {
        return $this->scholarship;
    }

    /**
     * @param $scholarship
     */
    public function setScholarship($scholarship)
    {
        $this->scholarship = $scholarship;
    }

    /**
     * Set dbSheetsUrl
     *
     * @param string $dbSheetsId
     *
     * @return FacultyPeriod
     */
    public function setDbSheetsId($dbSheetsId)
    {
        $this->dbSheetsId = $dbSheetsId;

        return $this;
    }

    /**
     * Get dbSheetsId
     *
     * @return string
     */
    public function getDbSheetsId()
    {
        return $this->dbSheetsId;
    }

    /**
     * Set groupsSheetId
     *
     * @param string $groupsSheetId
     *
     * @return FacultyPeriod
     */
    public function setGroupsSheetId($groupsSheetId)
    {
        $this->groupsSheetId = $groupsSheetId;

        return $this;
    }

    /**
     * Get groupsSheetId
     *
     * @return string
     */
    public function getGroupsSheetId()
    {
        return $this->groupsSheetId;
    }

    /**
     * Set subjectsSheetId
     *
     * @param string $subjectsSheetId
     *
     * @return FacultyPeriod
     */
    public function setSubjectsSheetId($subjectsSheetId)
    {
        $this->subjectsSheetId = $subjectsSheetId;

        return $this;
    }

    /**
     * Get subjectsSheetId
     *
     * @return string
     */
    public function getSubjectsSheetId()
    {
        return $this->subjectsSheetId;
    }

    /**
     * Set studentsSheetId
     *
     * @param string $studentsSheetId
     *
     * @return FacultyPeriod
     */
    public function setStudentsSheetId($studentsSheetId)
    {
        $this->studentsSheetId = $studentsSheetId;

        return $this;
    }

    /**
     * Get studentsSheetId
     *
     * @return string
     */
    public function getStudentsSheetId()
    {
        return $this->studentsSheetId;
    }

    /**
     * Set faculty
     *
     * @param \Maksi\BusinessBundle\Entity\Faculty $faculty
     *
     * @return FacultyPeriod
     */
    public function setFaculty(\Maksi\BusinessBundle\Entity\Faculty $faculty)
    {
        $this->faculty = $faculty;

        return $this;
    }

    /**
     * Get faculty
     *
     * @return \Maksi\BusinessBundle\Entity\Faculty
     */
    public function getFaculty()
    {
        return $this->faculty;
    }

    /**
     * Set period
     *
     * @param \Maksi\BusinessBundle\Entity\Period $period
     *
     * @return FacultyPeriod
     */
    public function setPeriod(\Maksi\BusinessBundle\Entity\Period $period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return \Maksi\BusinessBundle\Entity\Period
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @return mixed
     */
    public function getRatingSubjectLists()
    {
        return $this->ratingSubjectLists;
    }

    /**
     * @param mixed $ratingSubjectLists
     */
    public function setRatingSubjectLists($ratingSubjectLists)
    {
        $this->ratingSubjectLists = $ratingSubjectLists;
    }

    /**
     * @return mixed
     */
    public function getResultRatingSubjectLists()
    {
        return $this->resultRatingSubjectLists;
    }

    /**
     * @param mixed $resultRatingSubjectLists
     */
    public function setResultRatingSubjectLists($resultRatingSubjectLists)
    {
        $this->resultRatingSubjectLists = $resultRatingSubjectLists;
    }
}
