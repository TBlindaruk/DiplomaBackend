<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 04.02.17
 * Time: 17:34
 */

// src/AppBundle/Entity/Product.php
namespace Maksi\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="group")
 */
class Group
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $startYear;

    /**
     * @ORM\ManyToOne(targetEntity="Specialty", inversedBy="groups")
     * @ORM\JoinColumn(name="specialty_id", referencedColumnName="id")
     */
    private $specialty;

    /**
     * @ORM\ManyToOne(targetEntity="GroupType", inversedBy="groups")
     * @ORM\JoinColumn(name="group_type_id", referencedColumnName="id")
     */
    private $groupType;

    /**
     * @ORM\OneToMany(targetEntity="StudentPeriodGroup", mappedBy="group")
     */
    private $studentPeriodGroups;

    /**
     * @ORM\OneToMany(targetEntity="GroupPeriodSubject", mappedBy="group")
     */
    private $groupPeriodSubjects;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->studentPeriodGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupPeriodSubjects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startYear
     *
     * @param integer $startYear
     *
     * @return Group
     */
    public function setStartYear($startYear)
    {
        $this->startYear = $startYear;

        return $this;
    }

    /**
     * Get startYear
     *
     * @return integer
     */
    public function getStartYear()
    {
        return $this->startYear;
    }

    /**
     * Set specialty
     *
     * @param \Maksi\BusinessBundle\Entity\Specialty $specialty
     *
     * @return Group
     */
    public function setSpecialty(\Maksi\BusinessBundle\Entity\Specialty $specialty = null)
    {
        $this->specialty = $specialty;

        return $this;
    }

    /**
     * Get specialty
     *
     * @return \Maksi\BusinessBundle\Entity\Specialty
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }

    /**
     * Set groupType
     *
     * @param \Maksi\BusinessBundle\Entity\GroupType $groupType
     *
     * @return Group
     */
    public function setGroupType(\Maksi\BusinessBundle\Entity\GroupType $groupType = null)
    {
        $this->groupType = $groupType;

        return $this;
    }

    /**
     * Get groupType
     *
     * @return \Maksi\BusinessBundle\Entity\GroupType
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * Add studentPeriodGroup
     *
     * @param \Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup
     *
     * @return Group
     */
    public function addStudentPeriodGroup(\Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup)
    {
        $this->studentPeriodGroups[] = $studentPeriodGroup;

        return $this;
    }

    /**
     * Remove studentPeriodGroup
     *
     * @param \Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup
     */
    public function removeStudentPeriodGroup(\Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup)
    {
        $this->studentPeriodGroups->removeElement($studentPeriodGroup);
    }

    /**
     * Get studentPeriodGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentPeriodGroups()
    {
        return $this->studentPeriodGroups;
    }

    /**
     * Add groupPeriodSubject
     *
     * @param \Maksi\BusinessBundle\Entity\GroupPeriodSubject $groupPeriodSubject
     *
     * @return Group
     */
    public function addGroupPeriodSubject(\Maksi\BusinessBundle\Entity\GroupPeriodSubject $groupPeriodSubject)
    {
        $this->groupPeriodSubjects[] = $groupPeriodSubject;

        return $this;
    }

    /**
     * Remove groupPeriodSubject
     *
     * @param \Maksi\BusinessBundle\Entity\GroupPeriodSubject $groupPeriodSubject
     */
    public function removeGroupPeriodSubject(\Maksi\BusinessBundle\Entity\GroupPeriodSubject $groupPeriodSubject)
    {
        $this->groupPeriodSubjects->removeElement($groupPeriodSubject);
    }

    /**
     * Get groupPeriodSubjects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupPeriodSubjects()
    {
        return $this->groupPeriodSubjects;
    }
}
