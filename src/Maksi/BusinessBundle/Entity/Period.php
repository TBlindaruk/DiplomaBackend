<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 04.02.17
 * Time: 18:34
 */

namespace Maksi\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Maksi\BusinessBundle\Repository\PeriodRepository")
 * @ORM\Table(name="period")
 */
class Period{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="StudentPeriodGroup", mappedBy="period")
     */
    private $studentPeriodGroups;

    /**
     * @ORM\OneToMany(targetEntity="FacultyPeriod", mappedBy="period")
     */
    private $facultyPeriod;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->studentPeriodGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->facultyPeriod = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Period
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add studentPeriodGroup
     *
     * @param \Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup
     *
     * @return Period
     */
    public function addStudentPeriodGroup(\Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup)
    {
        $this->studentPeriodGroups[] = $studentPeriodGroup;

        return $this;
    }

    /**
     * Remove studentPeriodGroup
     *
     * @param \Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup
     */
    public function removeStudentPeriodGroup(\Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup)
    {
        $this->studentPeriodGroups->removeElement($studentPeriodGroup);
    }

    /**
     * Get studentPeriodGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentPeriodGroups()
    {
        return $this->studentPeriodGroups;
    }

    /**
     * Add facultyPeriod
     *
     * @param \Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod
     *
     * @return Period
     */
    public function addFacultyPeriod(\Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod)
    {
        $this->facultyPeriod[] = $facultyPeriod;

        return $this;
    }

    /**
     * Remove facultyPeriod
     *
     * @param \Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod
     */
    public function removeFacultyPeriod(\Maksi\BusinessBundle\Entity\FacultyPeriod $facultyPeriod)
    {
        $this->facultyPeriod->removeElement($facultyPeriod);
    }

    /**
     * Get facultyPeriod
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFacultyPeriod()
    {
        return $this->facultyPeriod;
    }
}
