<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 04.02.17
 * Time: 17:37
 */

namespace Maksi\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Maksi\BusinessBundle\Repository\SpecialtyRepository")
 * @ORM\Table(name="specialty")
 */
class Specialty{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Groups", mappedBy="specialty")
     */
    private $groups;

    /**
     * @ORM\ManyToOne(targetEntity="Faculty", inversedBy="specialties")
     * @ORM\JoinColumn(name="faculty_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $faculty;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Specialty
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add group
     *
     * @param \Maksi\BusinessBundle\Entity\Groups $group
     *
     * @return Specialty
     */
    public function addGroup(\Maksi\BusinessBundle\Entity\Groups $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \Maksi\BusinessBundle\Entity\Groups $group
     */
    public function removeGroup(\Maksi\BusinessBundle\Entity\Groups $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set faculty
     *
     * @param \Maksi\BusinessBundle\Entity\Faculty $faculty
     *
     * @return Specialty
     */
    public function setFaculty(\Maksi\BusinessBundle\Entity\Faculty $faculty = null)
    {
        $this->faculty = $faculty;

        return $this;
    }

    /**
     * Get faculty
     *
     * @return \Maksi\BusinessBundle\Entity\Faculty
     */
    public function getFaculty()
    {
        return $this->faculty;
    }
}
