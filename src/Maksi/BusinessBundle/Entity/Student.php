<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 04.02.17
 * Time: 18:32
 */

namespace Maksi\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="student")
 */
class Student{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="StudentPeriodGroup", mappedBy="student")
     */
    private $studentPeriodGroups;

    /**
     * @ORM\OneToMany(targetEntity="Student", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="Student", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRoot;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $src;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->studentPeriodGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Student
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add studentPeriodGroup
     *
     * @param \Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup
     *
     * @return Student
     */
    public function addStudentPeriodGroup(\Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup)
    {
        $this->studentPeriodGroups[] = $studentPeriodGroup;

        return $this;
    }

    /**
     * Remove studentPeriodGroup
     *
     * @param \Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup
     */
    public function removeStudentPeriodGroup(\Maksi\BusinessBundle\Entity\StudentPeriodGroup $studentPeriodGroup)
    {
        $this->studentPeriodGroups->removeElement($studentPeriodGroup);
    }

    /**
     * Get studentPeriodGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudentPeriodGroups()
    {
        return $this->studentPeriodGroups;
    }

    /**
     * Add child
     *
     * @param \Maksi\BusinessBundle\Entity\Student $child
     *
     * @return Student
     */
    public function addChild(\Maksi\BusinessBundle\Entity\Student $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Maksi\BusinessBundle\Entity\Student $child
     */
    public function removeChild(\Maksi\BusinessBundle\Entity\Student $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Maksi\BusinessBundle\Entity\Student $parent
     *
     * @return Student
     */
    public function setParent(\Maksi\BusinessBundle\Entity\Student $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Maksi\BusinessBundle\Entity\Student
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return mixed
     */
    public function getIsRoot()
    {
        return $this->isRoot;
    }

    /**
     * @param $isRoot
     *
     * @return $this
     */
    public function setIsRoot($isRoot)
    {
        $this->isRoot = $isRoot;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->src;
    }


    /**
     * @param $src
     *
     * @return $this
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }
}
