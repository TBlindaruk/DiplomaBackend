<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 04.02.17
 * Time: 19:18
 */

namespace Maksi\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="student_period_group")
 */
class StudentPeriodGroup{


    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Student", inversedBy="studentPeriodGroups")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    private $student;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Groups", inversedBy="studentPeriodGroups")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $group;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Period", inversedBy="studentPeriodGroups")
     * @ORM\JoinColumn(name="period_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $period;

    /**
     * Set student
     *
     * @param \Maksi\BusinessBundle\Entity\Student $student
     *
     * @return StudentPeriodGroup
     */
    public function setStudent(\Maksi\BusinessBundle\Entity\Student $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \Maksi\BusinessBundle\Entity\Student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set group
     *
     * @param \Maksi\BusinessBundle\Entity\Groups $group
     *
     * @return StudentPeriodGroup
     */
    public function setGroup(\Maksi\BusinessBundle\Entity\Groups $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Maksi\BusinessBundle\Entity\Groups
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set period
     *
     * @param \Maksi\BusinessBundle\Entity\Period $period
     *
     * @return StudentPeriodGroup
     */
    public function setPeriod(\Maksi\BusinessBundle\Entity\Period $period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return \Maksi\BusinessBundle\Entity\Period
     */
    public function getPeriod()
    {
        return $this->period;
    }
}
