<?php

namespace Maksi\GoogleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MaksiGoogleBundle:Default:index.html.twig');
    }
}
