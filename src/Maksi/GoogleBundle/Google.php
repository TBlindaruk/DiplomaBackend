<?php
/**
 * Created by PhpStorm.
 * User: tetiana
 * Date: 16.04.17
 * Time: 13:29
 */
namespace Maksi\GoogleBundle;

class Google
{
    /* @var \Google_Client */
    private $client;

    /* @var \Google_Service_Sheets */
    private $calendar;

    /**
     * Google constructor.
     * @param $scope
     */
    public function __construct($scope)
    {
        $this->client = new \Google_Client();
        $this->client->setApplicationName("Diploma");
//        $this->client->setDeveloperKey("AIzaSyAKl6ERGiCV4bBT5vK4ssnRY6gomJ8bSY4");
        $this->client->setAuthConfig('Diploma.json');
        $this->client->setScopes(implode(' ', array(
                \Google_Service_Sheets::SPREADSHEETS,
                \Google_Service_Drive::DRIVE)
        ));
    }

    /**
     * @return \Google_Client
     */
    public function getClient()
    {
        return $this->client;
    }


    /**
     * @return \Google_Service_Sheets
     */
    public function getSheets()
    {
        if (!$this->calendar) {
            $this->calendar = new \Google_Service_Sheets($this->client);
        }

        return $this->calendar;
    }
}