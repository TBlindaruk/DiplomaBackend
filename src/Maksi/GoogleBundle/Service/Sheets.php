<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 16.04.17
 * Time: 17:27
 */

namespace Maksi\GoogleBundle\Service;

use \Google_Service_Drive as Drive;
use \Google_Service_Sheets_Spreadsheet as Spreadsheet;
use \Google_Service_Sheets_Sheet as Sheet;
use \Google_Service_Sheets_SheetProperties as SheetProperties;
use \Google_Service_Sheets_SpreadsheetProperties as SpreadsheetProperties;
use \Google_Service_Drive_Permission as Permission;

use Maksi\BusinessBundle\Repository\GroupsRepository;
use Maksi\GoogleBundle\Google;
use Maksi\BusinessBundle\Entity\Groups;


use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
/**
 * Class Sheets
 */
class Sheets implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    const GOOGLE_SHEET_GROUPS_NAME = 'Групи';
    const GOOGLE_SHEET_SUBJECTS_NAME = 'Предмети';
    const GOOGLE_SHEET_STUDENTS_NAME = 'Студенти';

    /**
     * @var \Google_Client
     */
    private $_googleClient;

    /**
     * @var \Google_Service_Sheets
     */
    private $_sheets;

    /**
     * Sheets constructor.
     * @param $googleClient \Maksi\GoogleBundle\Google
     */
    public function __construct($googleClient)
    {
        $this->_googleClient = $googleClient->getClient();
        $this->_sheets = new \Google_Service_Sheets($this->_googleClient);
    }

    /**
     * @param $data
     *
     * @return \Google_Service_Sheets_Spreadsheet
     */
    public function createSheets($data)
    {
        /*create properties*/
        $properties = new SpreadsheetProperties();
        $properties->setTitle($data['name']);

        /*create Permission*/
        $newPermission = new Permission();
        $newPermission->setEmailAddress($data['email']);
        $newPermission->setType('user');
        $newPermission->setRole('writer');
        $optParams = array('sendNotificationEmail' => false);

        $sheetNames = array(
            static::GOOGLE_SHEET_GROUPS_NAME,
            static::GOOGLE_SHEET_STUDENTS_NAME,
            static::GOOGLE_SHEET_SUBJECTS_NAME
        );

        $sheets = array();

        foreach ($sheetNames as $sheetName) {
            $sheetProperties = new SheetProperties();
            $sheetProperties->setTitle($sheetName);
            $sheet = new Sheet();
            $sheet->setProperties($sheetProperties);
            $sheets[] = $sheet;
        }

        /*create Spreadsheet*/
        $requestBody = new Spreadsheet();
        $requestBody->setSheets($sheets);
        $requestBody->setProperties($properties);

        /*create spreadsheet in google*/
        $ss = $this->_sheets->spreadsheets->create($requestBody);

        /*create drive / save to drive*/
        $drive = new Drive($this->_googleClient);
        $drive->permissions->create($ss->getSpreadsheetId(), $newPermission, $optParams);

        return $ss;
    }

    /**
     * @param $data
     */
    public function changePermissionToRead($data)
    {
        /*create Permission*/
        $newPermission = new Permission();
        $newPermission->setEmailAddress($data['email']);
        $newPermission->setType('user');
        $newPermission->setRole('reader');
        $optParams = array('sendNotificationEmail' => false);

        /*create drive / save to drive*/
        $drive = new Drive($this->_googleClient);

        $spreadSheetId = $data['spreadSheetId'];

        /**@var $permission Permission */
        foreach ($drive->permissions->listPermissions($spreadSheetId) as $permission) {
            if ($permission->getRole() !== 'owner') {
                $drive->permissions->delete($spreadSheetId, $permission->getId());
            }
        }
        $drive->permissions->create($data['spreadSheetId'], $newPermission, $optParams);
    }

    /**
     * @param $data
     * @return array
     */
    public function createRatingSheets($data)
    {
        $subjects = $this->_sheets->spreadsheets_values->get($data['spreadSheetId'], "'Предмети'!A:A")->getValues();
        $subjectGroups = $this->_sheets->spreadsheets_values->get($data['spreadSheetId'], 'Предмети!B:B')->getValues();
        $subjectPriorities = $this->_sheets->spreadsheets_values->get($data['spreadSheetId'], 'Предмети!C:C')->getValues();

        $subjectsArray = array();
        if (($subjects && $subjectGroups && $subjectPriorities) && (count($subjects) === count($subjectGroups) && count($subjects) === count($subjectPriorities))) {
            foreach ($subjectGroups as $index => $subjectGroup) {
                $subjectGroup = $subjectGroup[0];
                if (!isset($subjectsArray[$subjectGroup])) {
                    $subjectsArray[$subjectGroup] = array();
                }
                $subjectsArray[$subjectGroup][] = array(
                    'name' => $subjects[$index][0],
                    'priority' => $subjectPriorities[$index][0]
                );
            }
        }

        $students = $this->_sheets->spreadsheets_values->get($data['spreadSheetId'], "'Студенти'!A:A")->getValues();
        $studentTypes = $this->_sheets->spreadsheets_values->get($data['spreadSheetId'], "'Студенти'!B:B")->getValues();
        $studentGroups = $this->_sheets->spreadsheets_values->get($data['spreadSheetId'], "'Студенти'!C:C")->getValues();

        $studentsArray = array();
        if (($students && $studentTypes && $studentGroups) && (count($students) === count($studentTypes) && count($students) === count($studentGroups))) {
            foreach ($studentGroups as $index => $studentGroup) {
                $studentGroup = $studentGroup[0];
                if (!isset($studentsArray[$studentGroup])) {
                    $studentsArray[$studentGroup] = array();
                }
                $studentsArray[$studentGroup][] = array(
                    'type' => $studentTypes[$index][0],
                    'name' => $students[$index][0]
                );
            }
        }

        $return = array();
        if ($subjectsArray && $studentsArray) {
            foreach ($subjectsArray as $groupName => $subjects) {
                $groupLists = array();
                $groupLists['group_name'] = $groupName;
                $groupLists['group_subjects'] = [];
                foreach (array_column($subjectsArray[$groupName], 'name') as $subjectIndex => $subjectName) {
                    $sheetTitle = "$groupName | $subjectName";

                    $spreadSheet = new Spreadsheet();
                    $spreadSheetProp = new SpreadsheetProperties();
                    $spreadSheetProp->setTitle($sheetTitle);
                    $spreadSheet->setProperties($spreadSheetProp);
                    $sp = $this->_sheets->spreadsheets->create($spreadSheet);

                    $newSpreadsheetId = $sp->getSpreadsheetId();

                    $value = array();
                    $valueRange = new \Google_Service_Sheets_ValueRange();
                    $value[] = array('', '', $subjectName);
                    $value[] = array('', '', $subjectsArray[$groupName][$subjectIndex]['priority']);

                    foreach ($studentsArray[$groupName] as $student) {
                        $value[] = ["values" => array($student['name'], $student['type'])];
                    }
                    $valueRange->setValues($value);
                    $this->_sheets->spreadsheets_values->append($newSpreadsheetId, "Sheet1", $valueRange, array('valueInputOption' => "RAW"));

                    $groupLists['group_subjects'][] = array(
                        'name' => $subjectName,
                        'sheetId' => $sp->getSpreadsheetId()
                    );

                    /*create Permission*/
                    $newPermission = new Permission();
                    $newPermission->setEmailAddress($data['email']);
                    $newPermission->setType('user');
                    $newPermission->setRole('writer');
                    $optParams = array(
                        'sendNotificationEmail' => false
                    );

                    /*create drive / save to drive*/
                    $drive = new Drive($this->_googleClient);

                    $drive->permissions->create($newSpreadsheetId, $newPermission, $optParams);
                }
                $return[] = $groupLists;
            }
        }

        return $return;
    }

    /**
     * @param $sheetData
     * @return array
     */
    public function createResultRatingSheets($sheetData)
    {
        $result = array();
        foreach ($sheetData as $index => $group) {
            $groupArray = array();
            $groupName = $group['group_name'];
            $groupArray['group_name'] = $groupName;
            $groupArray['group_subjects'] = $group['group_subjects'];
            $students = array();
            $totalRating = array();
            $coefficientSum = 0;
            foreach ($group['group_subjects'] as $subject) {
                $sheetId = $subject['sheetId'];
                if (!$students) {
                    $students = $this->_sheets->spreadsheets_values->get($sheetId, "'Sheet1'!A:A")->getValues();
                    foreach ($students as $index => $student) {
                        if (!isset($student[0])) {
                            $students[$index][0] = \Google_Model::NULL_VALUE;
                        }
                    }
                    $studentsTypes = $this->_sheets->spreadsheets_values->get($sheetId, "'Sheet1'!B:B")->getValues();
                    foreach ($studentsTypes as $index => $studentType) {
                        $type = isset($studentType[0]) ? $studentType[0] : \Google_Model::NULL_VALUE;
                        $students[$index][] = $type;
                    }
                }

                $studentsRatings = $this->_sheets->spreadsheets_values->get($sheetId, "'Sheet1'!C:C")->getValues();
                $coefficient = null;
                foreach ($students as $index => $student) {
                    $rating = isset($studentsRatings[$index][0]) ? $studentsRatings[$index][0] : \Google_Model::NULL_VALUE;
                    $students[$index][] = $rating;

                    if ($index === 1) {
                        $coefficient = $rating;
                        $coefficientSum += $coefficient;
                    }

                    if (!isset($totalRating[$index])) {
                        $totalRating[$index] = 0;
                    }

                    if ($index <= 1) {
                        $totalRating[$index] = \Google_Model::NULL_VALUE;
                    } else {
                        if ($totalRating[$index] !== \Google_Model::NULL_VALUE) {
                            if ($rating !== \Google_Model::NULL_VALUE && $rating >= 60) {
                                $totalRating[$index] = ($totalRating[$index] + $coefficient * $rating);
                            } else {
                                $totalRating[$index] = \Google_Model::NULL_VALUE;
                            }
                        }
                    }
                }
            }
            foreach ($students as $index => $student) {
                $rating = $totalRating[$index];
                if ($totalRating[$index] !== \Google_Model::NULL_VALUE && $coefficientSum !== 0) {
                    $rating = $totalRating[$index] / $coefficientSum;
                }
                array_splice($students[$index], 2, 0, $rating);
            }
            $groupArray['result_rating'] = $students;

            $spreadSheet = new Spreadsheet();
            $spreadSheetProp = new SpreadsheetProperties();
            $spreadSheetProp->setTitle($groupName);
            $spreadSheet->setProperties($spreadSheetProp);
            $sp = $this->_sheets->spreadsheets->create($spreadSheet);

            $newSpreadsheetId = $sp->getSpreadsheetId();

            $valueRange = new \Google_Service_Sheets_ValueRange();
            $valueRange->setValues($students);
            $this->_sheets->spreadsheets_values->append($newSpreadsheetId, "Sheet1", $valueRange, array('valueInputOption' => "RAW"));

            $groupArray['result_rating'] = $sp->getSpreadsheetId();

            /*create Permission*/
            $newPermission = new Permission();
            $newPermission->setEmailAddress('t.blindaruk@gmail.com');
            $newPermission->setType('user');
            $newPermission->setRole('reader');
            $optParams = array(
                'sendNotificationEmail' => false
            );

            /*create drive / save to drive*/
            $drive = new Drive($this->_googleClient);

            $drive->permissions->create($newSpreadsheetId, $newPermission, $optParams);
            $result[] = $groupArray;
        }
        return $result;
    }


    function findBudget($line)
    {
        if (isset($line[2])) {
            if ($line[2] == 'Б') {
                return true;
            }
        }
        return false;
    }

    function addGroupName($line, $groupName)
    {
        array_unshift($line, $groupName);
        return $line;
    }

    public function createGrantList($sheetId, $ratingLists)
    {

        $groups = $this->_sheets->spreadsheets_values->get($sheetId, "'Групи'!A:A")->getValues();
        $groupsUnions = $this->_sheets->spreadsheets_values->get($sheetId, 'Групи!B:B')->getValues();
        $groupsPercentage = $this->_sheets->spreadsheets_values->get($sheetId, 'Групи!C:C')->getValues();
        $groupHeightened = $this->_sheets->spreadsheets_values->get($sheetId, 'Групи!D:D')->getValues();
        $groupsArray = array();
        foreach ($groups as $index => $group) {
            $groupUnion = $groupsUnions[$index][0];
            if (!isset($groupsArray[$groupUnion])) {
                $groupsArray[$groupUnion] = array();
                $groupsArray[$groupUnion]['groups'] = array();
                $groupsArray[$groupUnion]['percentage'] = $groupsPercentage[$index][0];
                $groupsArray[$groupUnion]['heightened'] = $groupHeightened[$index][0];
                $groupsArray[$groupUnion]['students'] = array();
            }
            $groupsArray[$groupUnion]['groups'] [] = $group[0];
        }

        foreach ($groupsArray as $unionIndex => $union) {
            foreach ($union['groups'] as $group) {
                foreach ($ratingLists as $list) {
                    if ($group === $list['group_name']) {
                        $row = $this->_sheets->spreadsheets_values->get($list['result_rating'], 'Sheet1!A:C')->getValues();

                        $groupsArray[$unionIndex]['students'] =
                            array_merge(
                                $groupsArray[$unionIndex]['students'],
                                array_map(
                                    array($this, 'addGroupName'),
                                    $row,
                                    array_fill(
                                        0,
                                        count($row),
                                        $group
                                    )
                                )
                            );
                        break;
                    }
                }
            }
        }

        $betterStudents = [];
        foreach ($groupsArray as $unionIndex => $union) {

            $students = array_filter($groupsArray[$unionIndex]['students'],array($this, 'findBudget'));
            $studentsCount = count($students);

            if($studentsCount>2) {
                $scholarshipCount = round(($studentsCount * $groupsArray[$unionIndex]['percentage'] / 100));
                $higherScholarshipCount = round(($studentsCount * $groupsArray[$unionIndex]['heightened'] / 100));
                usort($students, array($this, 'sort'));

                $groupsArray[$unionIndex]['count'] = "$studentsCount | $scholarshipCount | $higherScholarshipCount";
                $studentsWithScholarship = [];
                $count = 0;
                $lastRatingHi = null;
                $lastRating = null;
                foreach ($students as $student) {
                    if (isset($student[3])) {
                        $student[3] = round($student[3], 2);
                        if ($count < $higherScholarshipCount) {
                            array_unshift($student, 'ПC');
                            array_push($studentsWithScholarship, $student);
                            $lastRatingHi = $student[4];
                            $betterStudents[] = $student;
                        } elseif ($lastRatingHi === $student[3]) {
                            array_unshift($student, 'ПC-');
                            array_push($studentsWithScholarship, $student);
                            $lastRatingHi = $student[4];
                            $betterStudents[] = $student;
                        } elseif ($count < $scholarshipCount) {
                            array_unshift($student, 'C');
                            array_push($studentsWithScholarship, $student);
                            $lastRating = $student[4];
                        } elseif ($lastRating === $student[3]) {
                            array_unshift($student, 'C-');
                            array_push($studentsWithScholarship, $student);
                            $lastRating = $student[4];
                        }

                        $count++;
                    } else {
                        break;
                    }
                }

                $groupsArray[$unionIndex]['students'] = $studentsWithScholarship;
            }else{
                $groupsArray[$unionIndex]['students'] = array();
                $groupsArray[$unionIndex]['count'] = "$studentsCount | 1 | 1";
                if($student = array_shift($students)){
                    
                    if(isset($student[3])){
                        $rating = round($student[3],2);
                        $student[3] = $rating;
                        if($rating>=90){
                            array_unshift($student, 'ПC');
                            $groupsArray[$unionIndex]['students'] = array($student);
                            $betterStudents[] = $student;
                        }elseif($rating>=74){
                            array_unshift($student, 'C');
                            $groupsArray[$unionIndex]['students'] = array($student);
                        }
                    }
                }
            }
        }

        $values = array();
        foreach ($groupsArray as $unionIndex => $union){
            $values[] = array(implode(' | ',$groupsArray[$unionIndex]['groups']),$groupsArray[$unionIndex]['count']);
            foreach ($groupsArray[$unionIndex]['students'] as $student){
                $values[] = $student;
            }
            $values[] = array();
        }


        $spreadSheet = new Spreadsheet();
        $spreadSheetProp = new SpreadsheetProperties();
        $spreadSheetProp->setTitle('Рейтинги');
        $spreadSheet->setProperties($spreadSheetProp);
        $sp = $this->_sheets->spreadsheets->create($spreadSheet);

        $newSpreadsheetId = $sp->getSpreadsheetId();

        $valueRange = new \Google_Service_Sheets_ValueRange();
        $valueRange->setValues($values);
        $this->_sheets->spreadsheets_values->append($newSpreadsheetId, "Sheet1", $valueRange, array('valueInputOption' => "RAW"));


        /*create Permission*/
        $newPermission = new Permission();
        $newPermission->setEmailAddress('t.blindaruk@gmail.com');
        $newPermission->setType('user');
        $newPermission->setRole('writer');
        $optParams = array(
            'sendNotificationEmail' => false
        );

        /*create drive / save to drive*/
        $drive = new Drive($this->_googleClient);

        $drive->permissions->create($newSpreadsheetId, $newPermission, $optParams);
        return array(
            'sheetId'=>$newSpreadsheetId,
            'students'=>$betterStudents
        );
    }

    function sort($a,$b){
        if(!isset($a[3])){
            return 1;
        }
        if(!isset($b[3])){
            return -1;
        }else{

            $a[3] = (float)$a[3];
            $b[3] = (float)$b[3];

            if($b[3]===$a[3]){
                return 0;
            }
            if($b[3]<$a[3]){
                return -1;
            }else{
                return 1;
            }
        }
    }

}