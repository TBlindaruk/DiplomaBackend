import { Component } from '@angular/core';

@Component({
    selector: 'pm-app',
    template: `
    <div>
        <nav class='navbar navbar-inverse bg-primary'>
            <div class='container-fluid'>
                <a  [routerLink]="['/']" class='navbar-brand'>{{pageTitle}}</a>
                <!--<ul class='nav navbar-nav'>-->
                    <!--<li><a [routerLink]="['/welcome']">Home</a></li>-->
                    <!--<li><a [routerLink]="['/products']">Product List</a></li>-->
                <!--</ul>-->
            </div>
        </nav>
        <div class='container'>
            <router-outlet></router-outlet>
        </div>
     </div>
     `
})
export class AppComponent {
    pageTitle: string = `Hall Of Fame`;
}
