"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
var welcome_component_1 = require("./examples/home/welcome.component");
var period_module_1 = require("./components/period/period.module");
var faculty_module_1 = require("./components/faculty/faculty.module");
var specialty_module_1 = require("./components/specialty/specialty.module");
var group_module_1 = require("./components/group/group.module");
var student_module_1 = require("./components/student/student.module");
var hall_of_fame_module_1 = require("./components/hall-of-fame/hall-of-fame.module");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot([
                { path: 'welcome', component: welcome_component_1.WelcomeComponent },
                { path: '', redirectTo: 'periods', pathMatch: 'full' },
                { path: '**', redirectTo: 'periods', pathMatch: 'full' }
            ]),
            period_module_1.PeriodModule,
            faculty_module_1.FacultyModule,
            specialty_module_1.SpecialtyModule,
            group_module_1.GroupModule,
            student_module_1.StudentModule,
            hall_of_fame_module_1.HallOfFameModule
        ],
        declarations: [
            app_component_1.AppComponent,
            welcome_component_1.WelcomeComponent
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map