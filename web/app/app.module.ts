import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {AppComponent}  from './app.component';
import {WelcomeComponent} from "./examples/home/welcome.component";
import {PeriodModule} from "./components/period/period.module";
import {FacultyModule} from "./components/faculty/faculty.module";
import {SpecialtyModule} from "./components/specialty/specialty.module";
import {GroupModule} from "./components/group/group.module";
import {StudentModule} from "./components/student/student.module";
import {HallOfFameModule} from "./components/hall-of-fame/hall-of-fame.module";


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule.forRoot([
            {path: 'welcome', component: WelcomeComponent},
            {path: '', redirectTo: 'periods', pathMatch: 'full'},
            {path: '**', redirectTo: 'periods', pathMatch: 'full'}
        ]),
        PeriodModule,
        FacultyModule,
        SpecialtyModule,
        GroupModule,
        StudentModule,
        HallOfFameModule
    ],
    declarations: [
        AppComponent,
        WelcomeComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
