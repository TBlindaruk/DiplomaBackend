"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var faculty_1 = require("../../../model/faculty/faculty");
var faculty_service_1 = require("../../../service/faculty/faculty.service");
var FacultyListComponent = (function () {
    function FacultyListComponent(_facultyService) {
        this._facultyService = _facultyService;
        this.faculties = [];
        this.faculty = new faculty_1.Faculty();
        this.isActive = true;
        this.newPeriod = '';
    }
    FacultyListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this._facultyService.getFaculties().subscribe(function (faculties) {
            for (var facultyIndex in faculties) {
                self.faculties.push(new faculty_1.Faculty(faculties[facultyIndex]));
            }
            self.isActive = false;
        }, function (error) { return _this.errorMessage = error; });
    };
    FacultyListComponent.prototype.onCreateFaculty = function (facultyForm) {
        var self = this, faculty = new faculty_1.Faculty(this.faculty);
        self.isActive = true;
        this._facultyService.addFaculty(faculty).subscribe(function (response) {
            faculty.id = response.id;
            self.faculties.unshift(faculty);
            self.faculty.clear();
            self.createFacultyModal.hide();
            self.isActive = false;
        });
    };
    FacultyListComponent.prototype.onDeleteFaculty = function (event) {
        for (var i = 0; i < this.faculties.length; ++i) {
            if (event.id == this.faculties[i].id) {
                this.faculties.splice(i, 1);
                break;
            }
        }
    };
    return FacultyListComponent;
}());
__decorate([
    core_1.ViewChild('createFacultyModal'),
    __metadata("design:type", ng2_bootstrap_1.ModalDirective)
], FacultyListComponent.prototype, "createFacultyModal", void 0);
FacultyListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './faculty-list.component.html',
        styleUrls: ['./faculty-list.component.css']
    }),
    __metadata("design:paramtypes", [faculty_service_1.FacultyService])
], FacultyListComponent);
exports.FacultyListComponent = FacultyListComponent;
//# sourceMappingURL=faculty-list.component.js.map