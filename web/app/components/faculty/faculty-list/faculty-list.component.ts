import {Component, OnInit, ViewChild} from "@angular/core";
import {ModalDirective} from "ng2-bootstrap";
import {Faculty} from "../../../model/faculty/faculty";
import {FacultyService} from "../../../service/faculty/faculty.service";

@Component({
    moduleId: module.id,
    templateUrl: './faculty-list.component.html',
    styleUrls: ['./faculty-list.component.css']
})

export class FacultyListComponent implements OnInit {

    selector: 'faculty-list';
    faculties: Faculty[] = [];
    faculty: Faculty = new Faculty();
    isActive: boolean = true;

    errorMessage: string;
    newPeriod: string = '';

    @ViewChild('createFacultyModal') createFacultyModal: ModalDirective;

    constructor(private _facultyService: FacultyService) {

    }

    ngOnInit(): void {
        var self = this;
        this._facultyService.getFaculties().subscribe(function (faculties) {
            for (let facultyIndex in faculties) {
                self.faculties.push(new Faculty(faculties[facultyIndex]));
            }
            self.isActive = false;
        }, error => this.errorMessage = <any> error);

    }

    onCreateFaculty(facultyForm: any): void {
        let self = this,
            faculty = new Faculty(this.faculty);
        self.isActive = true;
        this._facultyService.addFaculty(faculty).subscribe(function (response) {
            faculty.id = response.id;
            self.faculties.unshift(faculty);
            self.faculty.clear();
            self.createFacultyModal.hide();
            self.isActive = false;
        });
    }

    onDeleteFaculty(event: Faculty) {
        for (let i = 0; i < this.faculties.length; ++i) {
            if (event.id == this.faculties[i].id) {
                this.faculties.splice(i, 1);
                break;
            }
        }
    }
}