"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// import {PeriodListComponent} from "./period-list.component";
// import {PeriodService} from "../../model/period.service";
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var faculty_list_component_1 = require("./faculty-list.component");
var faculty_row_module_1 = require("../faculty-row/faculty-row.module");
var faculty_service_1 = require("../../../service/faculty/faculty.service");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var FacultyListModule = (function () {
    function FacultyListModule() {
    }
    return FacultyListModule;
}());
FacultyListModule = __decorate([
    core_1.NgModule({
        declarations: [
            faculty_list_component_1.FacultyListComponent
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            faculty_row_module_1.FacultyRowModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            loader_module_1.LoaderModule,
            router_1.RouterModule.forChild([
                { path: 'faculties', component: faculty_list_component_1.FacultyListComponent },
            ])
        ],
        providers: [faculty_service_1.FacultyService],
    })
], FacultyListModule);
exports.FacultyListModule = FacultyListModule;
//# sourceMappingURL=faculty-list.module.js.map