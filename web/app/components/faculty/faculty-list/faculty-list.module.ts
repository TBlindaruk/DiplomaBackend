import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

// import {PeriodListComponent} from "./period-list.component";
// import {PeriodService} from "../../model/period.service";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {FacultyListComponent} from "./faculty-list.component";
import {FacultyRowModule} from "../faculty-row/faculty-row.module";
import {FacultyService} from "../../../service/faculty/faculty.service";
import {LoaderModule} from "../../../core/components/loader/loader.module";

@NgModule({
    declarations: [
        FacultyListComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        FacultyRowModule,
        ModalModule.forRoot(),
        LoaderModule,
        RouterModule.forChild([
            {path: 'faculties', component: FacultyListComponent},
        ])
    ],
    providers: [FacultyService],
})
export class FacultyListModule {

}
