"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var faculty_1 = require("../../../model/faculty/faculty");
var faculty_service_1 = require("../../../service/faculty/faculty.service");
var FacultyRowComponent = (function () {
    function FacultyRowComponent(_facultyService) {
        this._facultyService = _facultyService;
        this.deleteFaculty = new core_1.EventEmitter();
        this.editMode = false;
        this.isActive = false;
    }
    FacultyRowComponent.prototype.changeMode = function () {
        this.editMode = !this.editMode;
    };
    FacultyRowComponent.prototype.edit = function () {
        var self = this, faculty = this.faculty;
        self.isActive = true;
        this._facultyService.editFaculty(faculty).subscribe(function (response) {
            self.changeMode();
            self.isActive = false;
        });
    };
    FacultyRowComponent.prototype.onDeleteFaculty = function () {
        var self = this;
        self.isActive = true;
        this._facultyService.deleteFaculty(this.faculty).subscribe(function () {
            self.deleteFaculty.emit(self.faculty);
            self.isActive = false;
        });
    };
    return FacultyRowComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", faculty_1.Faculty)
], FacultyRowComponent.prototype, "faculty", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], FacultyRowComponent.prototype, "deleteFaculty", void 0);
FacultyRowComponent = __decorate([
    core_1.Component({
        selector: '.faculty-row',
        moduleId: module.id,
        templateUrl: './faculty-row.component.html',
        styleUrls: ['./faculty-row.component.css']
    }),
    __metadata("design:paramtypes", [faculty_service_1.FacultyService])
], FacultyRowComponent);
exports.FacultyRowComponent = FacultyRowComponent;
//# sourceMappingURL=faculty-row.component.js.map