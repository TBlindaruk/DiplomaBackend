import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Faculty} from "../../../model/faculty/faculty";
import {FacultyService} from "../../../service/faculty/faculty.service";
import {isBuiltInAccessor} from "@angular/forms/src/directives/shared";

@Component({
    selector: '.faculty-row',
    moduleId: module.id,
    templateUrl: './faculty-row.component.html',
    styleUrls: ['./faculty-row.component.css']
})

export class FacultyRowComponent{

    @Input()
    faculty: Faculty;

    @Output()
    deleteFaculty: EventEmitter<Faculty> = new EventEmitter<Faculty>();

    editMode: boolean = false;

    isActive: boolean = false;

    constructor(private _facultyService: FacultyService){

    }

    changeMode(): void{
        this.editMode = !this.editMode;
    }

    edit():void {
        let self = this,
            faculty =this.faculty;
        self.isActive =true;
        this._facultyService.editFaculty(faculty).subscribe(function (response) {
            self.changeMode();
            self.isActive =false;
        });
    }

    onDeleteFaculty(){
        let self = this;
        self.isActive =true;
        this._facultyService.deleteFaculty(this.faculty).subscribe(function () {
            self.deleteFaculty.emit(self.faculty);
            self.isActive =false;
        });
    }
}