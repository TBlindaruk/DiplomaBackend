import {NgModule} from '@angular/core';
import {FacultyRowComponent} from "./faculty-row.component";
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {LoaderModule} from "../../../core/components/loader/loader.module";

@NgModule({
    declarations: [
        FacultyRowComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoaderModule
    ],
    exports:[
        FacultyRowComponent
    ]
})
export class FacultyRowModule {

}
