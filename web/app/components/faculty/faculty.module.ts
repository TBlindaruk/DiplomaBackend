import {NgModule} from '@angular/core';
// import {RouterModule} from '@angular/router';

// import {PeriodService} from "./model/period.service";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {FacultyListModule} from "./faculty-list/faculty-list.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),/*,*/
        FacultyListModule

        // PeriodViewModule,
        // PeriodListModule

    ],
    providers: [/*PeriodService*/]
})
export class FacultyModule {

}
