"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var group_service_1 = require("../../../service/group/group.service");
var group_1 = require("../../../model/group/group");
var specialty_service_1 = require("../../../service/specialty/specialty.service");
var specialty_1 = require("../../../model/specialty/specialty");
var group_type_1 = require("../../../model/group/group-type");
var GroupListComponent = (function () {
    function GroupListComponent(_groupService, _specialtyService) {
        this._groupService = _groupService;
        this._specialtyService = _specialtyService;
        this.isActive = true;
        this.groups = [];
        this.newPeriod = '';
        this.specialties = [];
        this.group = new group_1.Group();
        this.yearList = [];
        this.groupTypes = group_type_1.GroupType.toOptionArray();
    }
    GroupListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this._groupService.getGroups().subscribe(function (groups) {
            for (var groupIndex in groups) {
                self.groups.push(new group_1.Group(groups[groupIndex]));
            }
            if (self.specialties) {
                self.isActive = false;
            }
        }, function (error) { return _this.errorMessage = error; });
        this._specialtyService.getSpecialties().subscribe(function (specialties) {
            for (var specialtyIndex in specialties) {
                self.specialties.push(new specialty_1.Specialty(specialties[specialtyIndex]));
            }
            if (self.groups) {
                self.isActive = false;
            }
        });
        for (var i = 2000; i <= (new Date()).getFullYear(); ++i) {
            self.yearList.push(i);
        }
    };
    GroupListComponent.prototype.onCreateGroup = function (groupForm) {
        var self = this, group = new group_1.Group(this.group);
        self.isActive = true;
        this._groupService.addGroup(group).subscribe(function (response) {
            group.id = response.id;
            group.specialty = self.findSpecialtyById(group.specialty.id);
            self.groups.unshift(group);
            self.group.clear();
            self.createGroupModal.hide();
            self.isActive = false;
        });
    };
    GroupListComponent.prototype.findSpecialtyById = function (id) {
        for (var _i = 0, _a = this.specialties; _i < _a.length; _i++) {
            var specialty = _a[_i];
            if (specialty.id == id) {
                return specialty;
            }
        }
        return null;
    };
    GroupListComponent.prototype.onDeleteGroup = function (event) {
        for (var i = 0; i < this.groups.length; ++i) {
            if (event.id == this.groups[i].id) {
                this.groups.splice(i, 1);
                break;
            }
        }
    };
    return GroupListComponent;
}());
__decorate([
    core_1.ViewChild('createGroupModal'),
    __metadata("design:type", ng2_bootstrap_1.ModalDirective)
], GroupListComponent.prototype, "createGroupModal", void 0);
GroupListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './group-list.component.html',
        styleUrls: ['./group-list.component.css'],
        selector: 'group-list'
    }),
    __metadata("design:paramtypes", [group_service_1.GroupService,
        specialty_service_1.SpecialityService])
], GroupListComponent);
exports.GroupListComponent = GroupListComponent;
//# sourceMappingURL=group-list.component.js.map