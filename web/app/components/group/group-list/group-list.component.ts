import {Component, OnInit, ViewChild} from "@angular/core";
import {ModalDirective} from "ng2-bootstrap";
import {GroupService} from "../../../service/group/group.service";
import {Group} from "../../../model/group/group";
import {SpecialityService} from "../../../service/specialty/specialty.service";
import {Specialty} from "../../../model/specialty/specialty";
import {GroupType} from "../../../model/group/group-type";

@Component({
    moduleId: module.id,
    templateUrl: './group-list.component.html',
    styleUrls: ['./group-list.component.css'],
    selector: 'group-list'
})

export class GroupListComponent implements OnInit {

    isActive: boolean = true;

    groups: Group[] = [];
    errorMessage: string;
    newPeriod: string = '';

    specialties: Specialty[] = [];

    group: Group = new Group();
    yearList: number[] = [];
    groupTypes: {} = GroupType.toOptionArray();

    @ViewChild('createGroupModal') createGroupModal: ModalDirective;

    constructor(private _groupService: GroupService,
                private _specialtyService: SpecialityService) {

    }

    ngOnInit(): void {
        let self = this;
        this._groupService.getGroups().subscribe(function (groups) {
            for (let groupIndex in groups) {
                self.groups.push(new Group(groups[groupIndex]));
            }
            if (self.specialties) {
                self.isActive = false;
            }
        }, error => this.errorMessage = <any> error);


        this._specialtyService.getSpecialties().subscribe(function (specialties) {
            for (let specialtyIndex in specialties) {
                self.specialties.push(new Specialty(specialties[specialtyIndex]));
            }
            if (self.groups) {
                self.isActive = false;
            }
        });

        for (let i = 2000; i <= (new Date()).getFullYear(); ++i) {
            self.yearList.push(i);
        }
    }

    onCreateGroup(groupForm: any): void {
        let self = this,
            group = new Group(this.group);
        self.isActive = true;
        this._groupService.addGroup(group).subscribe(function (response) {
            group.id = response.id;
            group.specialty = self.findSpecialtyById(group.specialty.id);
            self.groups.unshift(group);
            self.group.clear();
            self.createGroupModal.hide();
            self.isActive = false;
        })
    }

    findSpecialtyById(id: number | string) {
        for (let specialty of this.specialties) {
            if (specialty.id == id) {
                return specialty;
            }
        }
        return null;
    }

    onDeleteGroup(event: Group) {
        for (let i = 0; i < this.groups.length; ++i) {
            if (event.id == this.groups[i].id) {
                this.groups.splice(i, 1);
                break;
            }
        }
    }
}