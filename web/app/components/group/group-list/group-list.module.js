"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var group_row_module_1 = require("../group-row/group-row.module");
var group_list_component_1 = require("./group-list.component");
var group_service_1 = require("../../../service/group/group.service");
var GroupListModule = (function () {
    function GroupListModule() {
    }
    return GroupListModule;
}());
GroupListModule = __decorate([
    core_1.NgModule({
        declarations: [
            group_list_component_1.GroupListComponent
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            group_row_module_1.GroupRowModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            loader_module_1.LoaderModule,
            router_1.RouterModule.forChild([
                { path: 'groups', component: group_list_component_1.GroupListComponent },
            ])
        ],
        providers: [group_service_1.GroupService],
    })
], GroupListModule);
exports.GroupListModule = GroupListModule;
//# sourceMappingURL=group-list.module.js.map