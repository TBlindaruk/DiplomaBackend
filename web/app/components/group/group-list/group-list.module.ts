import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {LoaderModule} from "../../../core/components/loader/loader.module";
import {GroupRowModule} from "../group-row/group-row.module";
import {GroupListComponent} from "./group-list.component";
import {GroupService} from "../../../service/group/group.service";

@NgModule({
    declarations: [
        GroupListComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        GroupRowModule,
        ModalModule.forRoot(),
        LoaderModule,
        RouterModule.forChild([
            {path: 'groups', component: GroupListComponent},
        ])
    ],
    providers: [GroupService],
})
export class GroupListModule {

}
