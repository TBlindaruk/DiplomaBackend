"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var group_1 = require("../../../model/group/group");
var group_type_1 = require("../../../model/group/group-type");
var group_service_1 = require("../../../service/group/group.service");
var GroupRowComponent = (function () {
    function GroupRowComponent(_groupService) {
        this._groupService = _groupService;
        this.deleteGroup = new core_1.EventEmitter();
        this.specialties = [];
        this.yearList = [];
        this.groupTypes = {};
        this.editMode = false;
        this.isActive = false;
    }
    GroupRowComponent.prototype.changeMode = function () {
        this.editMode = !this.editMode;
    };
    /**
     * @param value
     * @returns {string|null}
     */
    GroupRowComponent.prototype.findGroupNameByValue = function (value) {
        return group_type_1.GroupType.findNameByValue(value);
    };
    GroupRowComponent.prototype.edit = function () {
        var self = this, group = this.group;
        self.isActive = true;
        this._groupService.editGroup(group).subscribe(function (response) {
            self.changeMode();
            self.group = new group_1.Group(response);
            self.isActive = false;
        });
    };
    GroupRowComponent.prototype.onDeleteGroup = function () {
        var self = this;
        self.isActive = true;
        this._groupService.deleteGroup(this.group).subscribe(function () {
            self.deleteGroup.emit(self.group);
            self.isActive = false;
        });
    };
    return GroupRowComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", group_1.Group)
], GroupRowComponent.prototype, "group", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], GroupRowComponent.prototype, "deleteGroup", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], GroupRowComponent.prototype, "specialties", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], GroupRowComponent.prototype, "yearList", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], GroupRowComponent.prototype, "groupTypes", void 0);
GroupRowComponent = __decorate([
    core_1.Component({
        selector: '.group-row',
        moduleId: module.id,
        templateUrl: './group-row.component.html',
        styleUrls: ['./group-row.component.css']
    }),
    __metadata("design:paramtypes", [group_service_1.GroupService])
], GroupRowComponent);
exports.GroupRowComponent = GroupRowComponent;
//# sourceMappingURL=group-row.component.js.map