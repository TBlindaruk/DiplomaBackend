import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Group} from "../../../model/group/group";
import {GroupType} from "../../../model/group/group-type";
import {Specialty} from "../../../model/specialty/specialty";
import {GroupService} from "../../../service/group/group.service";

@Component({
    selector: '.group-row',
    moduleId: module.id,
    templateUrl: './group-row.component.html',
    styleUrls: ['./group-row.component.css']
})

export class GroupRowComponent {

    @Input()
    group: Group;

    @Output()
    deleteGroup: EventEmitter<Group> = new EventEmitter<Group>();

    @Input()
    specialties: Specialty[] = [];

    @Input()
    yearList: any[] = [];

    @Input()
    groupTypes: {} = {};

    editMode: boolean = false;

    isActive: boolean = false;

    constructor(private _groupService: GroupService) {

    }

    changeMode(): void {
        this.editMode = !this.editMode;
    }

    /**
     * @param value
     * @returns {string|null}
     */
    findGroupNameByValue(value: number) {
        return GroupType.findNameByValue(value);
    }

    edit(): void {
        let self = this,
            group = this.group;
        self.isActive = true;
        this._groupService.editGroup(group).subscribe(function (response) {
            self.changeMode();
            self.group = new Group(response);
            self.isActive = false;
        })
    }

    onDeleteGroup(){
        let self = this;
        self.isActive =true;
        this._groupService.deleteGroup(this.group).subscribe(function () {
            self.deleteGroup.emit(self.group);
            self.isActive =false;
        });
    }
}