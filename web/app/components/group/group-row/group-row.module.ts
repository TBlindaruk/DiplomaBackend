import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {LoaderModule} from "../../../core/components/loader/loader.module";
import {GroupRowComponent} from "./group-row.component";

@NgModule({
    declarations: [
        GroupRowComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoaderModule
    ],
    exports:[
        GroupRowComponent
    ]
})
export class GroupRowModule {

}
