import {NgModule} from '@angular/core';

import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {GroupListModule} from "./group-list/group-list.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),/*,*/
        GroupListModule

    ],
    providers: [/*PeriodService*/]
})
export class GroupModule {

}
