"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var period_service_1 = require("../../service/period/period.service");
var period_1 = require("../../model/period/period");
var faculty_service_1 = require("../../service/faculty/faculty.service");
var faculty_1 = require("../../model/faculty/faculty");
var HallOfFameComponent = (function () {
    function HallOfFameComponent(_periodService, _facultyService) {
        this._periodService = _periodService;
        this._facultyService = _facultyService;
        this.loading = true;
        this.periods = [];
        this.selectedPeriodId = null;
        this.period = new period_1.Period();
        this.faculties = [];
    }
    HallOfFameComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this._periodService.getPeriods().subscribe(function (periods) {
            for (var periodIndex in periods) {
                var period = new period_1.Period(periods[periodIndex]);
                if (!self.selectedPeriodId) {
                    self.selectedPeriodId = period.id;
                    self.period = period;
                }
                self.periods.push(period);
            }
            self.loading = false;
        }, function (error) { return _this.errorMessage = error; });
        this._facultyService.getFaculties().subscribe(function (faculties) {
            for (var indexFaculty in faculties) {
                self.faculties.push(new faculty_1.Faculty(faculties[indexFaculty]));
            }
        });
    };
    HallOfFameComponent.prototype.changePeriod = function ($event) {
        for (var i = 0; i < this.periods.length; ++i) {
            if (this.periods[i].id == $event.target.value) {
                this.period = this.periods[i];
                break;
            }
        }
    };
    return HallOfFameComponent;
}());
HallOfFameComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './hall-of-fame.component.html',
        styleUrls: ['./hall-of-fame.component.css'],
        selector: 'hall-of-fame'
    }),
    __metadata("design:paramtypes", [period_service_1.PeriodService,
        faculty_service_1.FacultyService])
], HallOfFameComponent);
exports.HallOfFameComponent = HallOfFameComponent;
//# sourceMappingURL=hall-of-fame.component.js.map