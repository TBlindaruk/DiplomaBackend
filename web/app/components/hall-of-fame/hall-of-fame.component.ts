import {Component, OnInit, ViewChild} from "@angular/core";


import {ModalDirective} from "ng2-bootstrap";

import {PeriodService} from '../../service/period/period.service';
import {Period} from '../../model/period/period';
import {FacultyService} from "../../service/faculty/faculty.service";
import {Faculty} from "../../model/faculty/faculty";

@Component({
    moduleId: module.id,
    templateUrl: './hall-of-fame.component.html',
    styleUrls: ['./hall-of-fame.component.css'],
    selector: 'hall-of-fame'
})

export class HallOfFameComponent implements OnInit{
    errorMessage: string;
    loading: boolean = true;
    periods: Period[] = [];
    selectedPeriodId: number | string = null;
    period: Period = new Period();
    faculties:Faculty[] = [];
    constructor(private _periodService: PeriodService,
                private _facultyService:FacultyService) {

    }

    ngOnInit(): void {
        let self = this;
        this._periodService.getPeriods().subscribe(
            function (periods) {
                for (let periodIndex in periods) {
                    let period = new Period(periods[periodIndex]);
                    if(!self.selectedPeriodId) {
                        self.selectedPeriodId = period.id;
                        self.period = period;
                    }
                    self.periods.push(period);
                }
                self.loading = false;
            },
            error => this.errorMessage = <any> error
        );

        this._facultyService.getFaculties().subscribe(function (faculties) {
            for(let indexFaculty in faculties){
                self.faculties.push(new Faculty(faculties[indexFaculty]));
            }
        });
    }
    changePeriod($event:any){
        for(let i =0;i<this.periods.length;++i){
            if(this.periods[i].id==$event.target.value){
                this.period= this.periods[i];
                break;
            }
        }
    }
}