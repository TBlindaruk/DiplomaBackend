import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {LoaderModule} from "../../core/components/loader/loader.module";
import {HallOfFameComponent} from './hall-of-fame.component';
import {RouterModule} from "@angular/router";
import {PeriodSubjectRatingListModule} from "../period/period-subject-rating-list/period-subject-rating-list.module";

@NgModule({
    declarations: [
        HallOfFameComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoaderModule,
        RouterModule.forChild([
            {path: 'halloffame', component: HallOfFameComponent},
        ]),
        PeriodSubjectRatingListModule
    ],
    exports:[
        HallOfFameComponent
    ]
})
export class HallOfFameModule {

}
