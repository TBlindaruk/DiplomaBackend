"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var period_settings_1 = require("../../../settings/period.settings");
var period_1 = require("../../../model/period/period");
var faculty_1 = require("../../../model/faculty/faculty");
var PeriodFacultyDbComponent = (function () {
    function PeriodFacultyDbComponent() {
        this.period = new period_1.Period();
        this.faculty = new faculty_1.Faculty();
        this.requestUrl = null;
        this.buttonName = "Створити таблиці для заповнення даними";
    }
    PeriodFacultyDbComponent.prototype.onLoadPeriod = function (period) {
        var self = this;
        self.period = period;
        if (self.faculty.id) {
            self.requestUrl = period_settings_1.PeriodSettings.API_PERIOD_FACULTY_CREATE_GOOGLE_SHEETS_DB(period.id, self.faculty.id);
        }
    };
    PeriodFacultyDbComponent.prototype.onLoadFaculty = function (faculty) {
        var self = this;
        self.faculty = faculty;
        if (self.period.id) {
            self.requestUrl = period_settings_1.PeriodSettings.API_PERIOD_FACULTY_CREATE_GOOGLE_SHEETS_DB(self.period.id, faculty.id);
        }
    };
    return PeriodFacultyDbComponent;
}());
PeriodFacultyDbComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './period-faculty-db.component.html',
        styleUrls: ['./period-faculty-db.component.css'],
        selector: 'period-faculty-db'
    })
], PeriodFacultyDbComponent);
exports.PeriodFacultyDbComponent = PeriodFacultyDbComponent;
//# sourceMappingURL=period-faculty-db.component.js.map