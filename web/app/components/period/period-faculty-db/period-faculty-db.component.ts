import {Component, OnInit, ViewChild} from "@angular/core";
import {PeriodSettings} from "../../../settings/period.settings";
import {Period} from "../../../model/period/period";
import {Faculty} from "../../../model/faculty/faculty";


@Component({
    moduleId: module.id,
    templateUrl: './period-faculty-db.component.html',
    styleUrls: ['./period-faculty-db.component.css'],
    selector: 'period-faculty-db'
})

export class PeriodFacultyDbComponent{

    period: Period = new Period();
    faculty: Faculty = new Faculty();
    requestUrl: string = null;
    buttonName: string = "Створити таблиці для заповнення даними";

    onLoadPeriod(period: Period){
        let self = this;
        self.period = period;
        if(self.faculty.id) {
            self.requestUrl = PeriodSettings.API_PERIOD_FACULTY_CREATE_GOOGLE_SHEETS_DB(period.id,self.faculty.id);
        }
    }

    onLoadFaculty(faculty: Faculty){
        let self = this;
        self.faculty = faculty;
        if(self.period.id) {
            self.requestUrl = PeriodSettings.API_PERIOD_FACULTY_CREATE_GOOGLE_SHEETS_DB(self.period.id,faculty.id);
        }
    }
}