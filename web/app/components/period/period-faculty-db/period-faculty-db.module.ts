import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {PeriodLayoutModule} from "../period-layout/period-layout.module";
import {GoogleSheetsModule} from "../../../core/components/google-sheets/google-sheets.module";
import {PeriodFacultyDbComponent} from "./period-faculty-db.component";


@NgModule({
    declarations: [
        PeriodFacultyDbComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        PeriodLayoutModule,
        GoogleSheetsModule,
        RouterModule.forChild([
            {path: 'period/:id/faculty/:facultyId/db', component: PeriodFacultyDbComponent}
        ]),
    ],
    providers: [/*PeriodService*/],
    exports:[
        PeriodFacultyDbComponent
    ]
})
export class PeriodFacultyDbModule {

}
