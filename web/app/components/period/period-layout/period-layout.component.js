"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var period_1 = require("../../../model/period/period");
var period_service_1 = require("../../../service/period/period.service");
var faculty_1 = require("../../../model/faculty/faculty");
var faculty_service_1 = require("../../../service/faculty/faculty.service");
var PeriodLayoutComponent = (function () {
    function PeriodLayoutComponent(_route, _router, _periodService, _facultyService) {
        this._route = _route;
        this._router = _router;
        this._periodService = _periodService;
        this._facultyService = _facultyService;
        this.period = new period_1.Period();
        this.faculty = new faculty_1.Faculty();
        this.disable = false;
        this.activeTab = null;
        this.loadPeriod = new core_1.EventEmitter();
        this.loadFaculty = new core_1.EventEmitter();
        this.isActive = true;
    }
    PeriodLayoutComponent.prototype.ngOnInit = function () {
        var self = this, facultyId;
        self.id = +self._route.snapshot.params['id'];
        if (facultyId = +self._route.snapshot.params['facultyId']) {
            self.facultyId = facultyId;
        }
        self._periodService.getPeriod(self.id).subscribe(function (response) {
            self.period.id = +response.id;
            self.period.name = response.name;
            self.period.sheetsDbId = self.getDSheetsId(response);
            self.period.sheetsRatingId = self.getDBRatingId(response);
            self.period.scholarship = self.getScholarship(response);
            self.loadPeriod.emit(self.period);
            if (self.facultyId) {
                self.period.facultyId = self.facultyId;
                if (self.faculty.id) {
                    self.isActive = false;
                }
            }
            else {
                self.isActive = false;
            }
        });
        if (self.facultyId) {
            self._facultyService.getFaculty(self.facultyId).subscribe(function (response) {
                self.faculty = new faculty_1.Faculty(response);
                self.loadFaculty.emit(self.faculty);
                if (self.period.id) {
                    self.isActive = false;
                }
            });
        }
    };
    PeriodLayoutComponent.prototype.getDSheetsId = function (response) {
        var self = this, facultyPeriods = response['faculty_period'], result = null;
        for (var _i = 0, facultyPeriods_1 = facultyPeriods; _i < facultyPeriods_1.length; _i++) {
            var facultyPeriod = facultyPeriods_1[_i];
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.db_sheets_id;
                break;
            }
        }
        return result;
    };
    PeriodLayoutComponent.prototype.getScholarship = function (response) {
        var self = this, facultyPeriods = response['faculty_period'], result = null;
        for (var _i = 0, facultyPeriods_2 = facultyPeriods; _i < facultyPeriods_2.length; _i++) {
            var facultyPeriod = facultyPeriods_2[_i];
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.scholarship;
                break;
            }
        }
        return result;
    };
    PeriodLayoutComponent.prototype.getDBRatingId = function (response) {
        var self = this, facultyPeriods = response['faculty_period'], result = null;
        for (var _i = 0, facultyPeriods_3 = facultyPeriods; _i < facultyPeriods_3.length; _i++) {
            var facultyPeriod = facultyPeriods_3[_i];
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.rating_subject_lists;
                if (facultyPeriod.result_rating_subject_lists) {
                    result = facultyPeriod.result_rating_subject_lists;
                }
                if (result) {
                    result = JSON.parse(result);
                }
                else {
                    result = null;
                }
                break;
            }
        }
        return result;
    };
    return PeriodLayoutComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PeriodLayoutComponent.prototype, "facultyId", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], PeriodLayoutComponent.prototype, "disable", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PeriodLayoutComponent.prototype, "activeTab", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], PeriodLayoutComponent.prototype, "loadPeriod", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], PeriodLayoutComponent.prototype, "loadFaculty", void 0);
PeriodLayoutComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './period-layout.component.html',
        styleUrls: ['./period-layout.component.css'],
        selector: 'period-layout'
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute,
        router_1.Router,
        period_service_1.PeriodService,
        faculty_service_1.FacultyService])
], PeriodLayoutComponent);
exports.PeriodLayoutComponent = PeriodLayoutComponent;
//# sourceMappingURL=period-layout.component.js.map