import {Component, OnInit, Output, EventEmitter, Input, Injectable} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Period} from "../../../model/period/period";
import {PeriodService} from "../../../service/period/period.service";
import {Faculty} from "../../../model/faculty/faculty";
import {FacultyService} from "../../../service/faculty/faculty.service";
import {ajaxGetJSON} from "rxjs/observable/dom/AjaxObservable";


@Component({
    moduleId: module.id,
    templateUrl: './period-layout.component.html',
    styleUrls: ['./period-layout.component.css'],
    selector: 'period-layout'
})

export class PeriodLayoutComponent implements OnInit {

    id: number;
    period: Period = new Period();

    @Input()
    facultyId: number;
    faculty: Faculty = new Faculty();

    @Input()
    disable: Boolean = false;

    @Input()
    activeTab: number = null;

    @Output()
    loadPeriod: EventEmitter<Period> = new EventEmitter<Period>();

    @Output()
    loadFaculty: EventEmitter<Faculty> = new EventEmitter<Faculty>();

    isActive: boolean = true;

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _periodService: PeriodService,
                private _facultyService: FacultyService) {

    }

    ngOnInit(): void {
        let self = this,
            facultyId: any;

        self.id = +self._route.snapshot.params['id'];
        if (facultyId = +self._route.snapshot.params['facultyId']) {
            self.facultyId = facultyId;
        }

        self._periodService.getPeriod(self.id).subscribe(function (response) {
            self.period.id = +response.id;
            self.period.name = response.name;
            self.period.sheetsDbId = self.getDSheetsId(response);
            self.period.sheetsRatingId = self.getDBRatingId(response);
            self.period.scholarship = self.getScholarship(response);
            self.loadPeriod.emit(self.period);
            if (self.facultyId) {
                self.period.facultyId=self.facultyId;
                if (self.faculty.id) {
                    self.isActive = false;
                }
            } else {
                self.isActive = false;
            }

        });

        if (self.facultyId) {
            self._facultyService.getFaculty(self.facultyId).subscribe(function (response) {
                self.faculty = new Faculty(response);
                self.loadFaculty.emit(self.faculty);
                if (self.period.id) {
                    self.isActive = false;
                }
            })
        }
    }

    protected getDSheetsId(response: any) {
        let self = this,
            facultyPeriods = response['faculty_period'],
            result: any = null;

        for (let facultyPeriod of facultyPeriods) {
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.db_sheets_id;
                break;
            }
        }
        return result;
    }

    protected getScholarship(response: any){
        let self = this,
            facultyPeriods = response['faculty_period'],
            result: any = null;

        for (let facultyPeriod of facultyPeriods) {
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.scholarship;
                break;
            }
        }
        return result;
    }

    protected getDBRatingId(response: any) {
        let self = this,
            facultyPeriods = response['faculty_period'],
            result: any = null;

        for (let facultyPeriod of facultyPeriods) {
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.rating_subject_lists;
                if(facultyPeriod.result_rating_subject_lists){
                    result = facultyPeriod.result_rating_subject_lists;
                }
                if (result) {
                    result = JSON.parse(result);
                } else {
                    result = null;
                }
                break;
            }
        }
        return result;
    }
}