"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var period_layout_component_1 = require("./period-layout.component");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var PeriodLayoutModule = (function () {
    function PeriodLayoutModule() {
    }
    return PeriodLayoutModule;
}());
PeriodLayoutModule = __decorate([
    core_1.NgModule({
        declarations: [
            period_layout_component_1.PeriodLayoutComponent
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            router_1.RouterModule,
            loader_module_1.LoaderModule
        ],
        providers: [],
        exports: [
            period_layout_component_1.PeriodLayoutComponent
        ]
    })
], PeriodLayoutModule);
exports.PeriodLayoutModule = PeriodLayoutModule;
//# sourceMappingURL=period-layout.module.js.map