import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {PeriodLayoutComponent} from "./period-layout.component";
import {LoaderModule} from "../../../core/components/loader/loader.module";

@NgModule({
    declarations: [
        PeriodLayoutComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        LoaderModule
    ],
    providers: [/*PeriodService*/],
    exports:[
        PeriodLayoutComponent
    ]
})
export class PeriodLayoutModule {

}
