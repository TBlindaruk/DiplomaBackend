"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var period_1 = require("../../../model/period/period");
var period_service_1 = require("../../../service/period/period.service");
var PeriodListComponent = (function () {
    function PeriodListComponent(_periodService) {
        this._periodService = _periodService;
        this.periods = [];
        this.loading = true;
        this.newPeriod = '';
    }
    PeriodListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this._periodService.getPeriods().subscribe(function (periods) {
            for (var periodIndex in periods) {
                self.periods.push(new period_1.Period(periods[periodIndex]));
            }
            self.loading = false;
        }, function (error) { return _this.errorMessage = error; });
    };
    PeriodListComponent.prototype.onCreatePeriod = function (periodValidation) {
        var self = this;
        this.newPeriod = this.newPeriod.trim();
        var period = { name: this.newPeriod, id: null };
        if (periodValidation.errors || !this.newPeriod) {
            this.staticModal.show();
        }
        else {
            this._periodService.addPeriod(period).subscribe(function (response) {
                period.id = response.id;
                self.periods.unshift(period);
                self.newPeriod = '';
            });
        }
    };
    return PeriodListComponent;
}());
__decorate([
    core_1.ViewChild('staticModal'),
    __metadata("design:type", ng2_bootstrap_1.ModalDirective)
], PeriodListComponent.prototype, "staticModal", void 0);
PeriodListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './period-list.component.html',
        styleUrls: ['./period-list.component.css']
    }),
    __metadata("design:paramtypes", [period_service_1.PeriodService])
], PeriodListComponent);
exports.PeriodListComponent = PeriodListComponent;
//# sourceMappingURL=period-list.component.js.map