import {Component, OnInit, ViewChild} from "@angular/core";
import {ModalDirective} from "ng2-bootstrap";
import {Period} from "../../../model/period/period";
import {PeriodService} from "../../../service/period/period.service";


@Component({
    moduleId: module.id,
    templateUrl: './period-list.component.html',
    styleUrls: ['./period-list.component.css']
})

export class PeriodListComponent implements OnInit {
    selector: 'period-list';
    periods: Period[] = [];
    errorMessage: string;
    loading: boolean = true;
    newPeriod: string = '';
    @ViewChild('staticModal') staticModal: ModalDirective;

    constructor(private _periodService: PeriodService) {

    }

    ngOnInit(): void {
        let self = this;
        this._periodService.getPeriods().subscribe(
            function (periods) {
                for(let periodIndex in periods) {
                    self.periods.push(new Period(periods[periodIndex]));
                }
                self.loading = false;
            },
            error => this.errorMessage = <any> error
        );
    }

    onCreatePeriod(periodValidation: any): void {
        let self = this;
        this.newPeriod = this.newPeriod.trim();
        let period = <Period>{name: this.newPeriod, id: null};
        if (periodValidation.errors || !this.newPeriod) {
            this.staticModal.show();
        } else {
            this._periodService.addPeriod(period).subscribe(function (response) {
                period.id = response.id;
                self.periods.unshift(period);
                self.newPeriod = '';
            });
        }
    }
}