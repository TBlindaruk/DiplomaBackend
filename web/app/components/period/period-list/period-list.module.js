"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var period_list_component_1 = require("./period-list.component");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var period_service_1 = require("../../../service/period/period.service");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var PeriodListModule = (function () {
    function PeriodListModule() {
    }
    return PeriodListModule;
}());
PeriodListModule = __decorate([
    core_1.NgModule({
        declarations: [
            period_list_component_1.PeriodListComponent
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            router_1.RouterModule.forChild([
                { path: 'periods', component: period_list_component_1.PeriodListComponent },
            ]),
            loader_module_1.LoaderModule
        ],
        providers: [period_service_1.PeriodService]
    })
], PeriodListModule);
exports.PeriodListModule = PeriodListModule;
//# sourceMappingURL=period-list.module.js.map