import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {PeriodListComponent} from "./period-list.component";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {PeriodService} from "../../../service/period/period.service";
import {LoaderModule} from "../../../core/components/loader/loader.module";

@NgModule({
    declarations: [
        PeriodListComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        RouterModule.forChild([
            {path: 'periods', component: PeriodListComponent},
        ]),
        LoaderModule
    ],
    providers: [PeriodService]
})
export class PeriodListModule {

}
