"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var period_settings_1 = require("../../../settings/period.settings");
var period_1 = require("../../../model/period/period");
var faculty_1 = require("../../../model/faculty/faculty");
var PeriodScholarshipComponent = (function () {
    function PeriodScholarshipComponent() {
        this.period = new period_1.Period();
        this.faculty = new faculty_1.Faculty();
        this.requestUrl = null;
        this.buttonName = "Створити рейтинг студентів";
        this.showButton = true;
    }
    PeriodScholarshipComponent.prototype.onLoadPeriod = function (period) {
        var self = this;
        self.period = period;
        if (self.faculty.id) {
            self.requestUrl = period_settings_1.PeriodSettings.API_PERIOD_FACULTY_CREATE_SCHOLARSHIP_DB(period.id, self.faculty.id);
        }
    };
    PeriodScholarshipComponent.prototype.onLoadFaculty = function (faculty) {
        var self = this;
        self.faculty = faculty;
        if (self.period.id) {
            self.requestUrl = period_settings_1.PeriodSettings.API_PERIOD_FACULTY_CREATE_SCHOLARSHIP_DB(self.period.id, faculty.id);
        }
    };
    return PeriodScholarshipComponent;
}());
PeriodScholarshipComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './period-scholarship.component.html',
        styleUrls: ['./period-scholarship.component.css'],
        selector: 'period-faculty-db'
    })
], PeriodScholarshipComponent);
exports.PeriodScholarshipComponent = PeriodScholarshipComponent;
//# sourceMappingURL=period-scholarship.component.js.map