import {Component, OnInit, ViewChild} from "@angular/core";
import {PeriodSettings} from "../../../settings/period.settings";
import {Period} from "../../../model/period/period";
import {Faculty} from "../../../model/faculty/faculty";


@Component({
    moduleId: module.id,
    templateUrl: './period-scholarship.component.html',
    styleUrls: ['./period-scholarship.component.css'],
    selector: 'period-faculty-db'
})

export class PeriodScholarshipComponent{

    period: Period = new Period();
    faculty: Faculty = new Faculty();
    requestUrl: string = null;
    buttonName: string = "Створити рейтинг студентів";

    showButton:boolean = true;
    onLoadPeriod(period: Period){
        let self = this;
        self.period = period;
        if(self.faculty.id) {
            self.requestUrl = PeriodSettings.API_PERIOD_FACULTY_CREATE_SCHOLARSHIP_DB(period.id,self.faculty.id);
        }
    }

    onLoadFaculty(faculty: Faculty){
        let self = this;
        self.faculty = faculty;
        if(self.period.id) {
            self.requestUrl = PeriodSettings.API_PERIOD_FACULTY_CREATE_SCHOLARSHIP_DB(self.period.id,faculty.id);
        }
    }
}