import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {PeriodLayoutModule} from "../period-layout/period-layout.module";
import {GoogleSheetsModule} from "../../../core/components/google-sheets/google-sheets.module";
import {PeriodScholarshipComponent} from "./period-scholarship.component";


@NgModule({
    declarations: [
        PeriodScholarshipComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        PeriodLayoutModule,
        GoogleSheetsModule,
        RouterModule.forChild([
            {path: 'period/:id/faculty/:facultyId/scholarship', component: PeriodScholarshipComponent}
        ]),
    ],
    providers: [/*PeriodService*/],
    exports:[
        PeriodScholarshipComponent
    ]
})
export class PeriodScholarshipModule {

}
