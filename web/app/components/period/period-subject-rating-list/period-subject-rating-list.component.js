"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var period_service_1 = require("../../../service/period/period.service");
var period_1 = require("../../../model/period/period");
var faculty_1 = require("../../../model/faculty/faculty");
var period_settings_1 = require("../../../settings/period.settings");
var PeriodSubjectRatingListComponent = (function () {
    function PeriodSubjectRatingListComponent(_periodService) {
        this._periodService = _periodService;
        this.ratingSheetIds = null;
        this.requestUrl = null;
        this.period = new period_1.Period();
        this.faculty = new faculty_1.Faculty();
        this.isActive = false;
        this.activeGroup = null;
        this.activeSubject = null;
        this.isPrivate = true;
    }
    PeriodSubjectRatingListComponent.prototype.createSubjectRatingList = function () {
        var self = this;
        self.isActive = true;
        self._periodService.createGoogleSheets(self.requestUrl).subscribe(function (responce) {
            self.ratingSheetIds = responce;
            self.isActive = false;
        });
    };
    PeriodSubjectRatingListComponent.prototype.createResultRatingSheets = function () {
        var self = this;
        self.isActive = true;
        var url = period_settings_1.PeriodSettings.API_PERIOD_FACULTY_CREATE_RESULT_RATING_DB(self.period.id, self.faculty.id);
        self._periodService.createGoogleSheets(url).subscribe(function (responce) {
            self.ratingSheetIds = responce;
            for (var _i = 0, _a = self.ratingSheetIds; _i < _a.length; _i++) {
                var group = _a[_i];
                if (self.activeGroup['group_name'] === group['group_name']) {
                    self.activeGroup = group;
                    self.activeSubject = null;
                    break;
                }
            }
            self.isActive = false;
        });
    };
    PeriodSubjectRatingListComponent.prototype.onLoadPeriod = function (period) {
        var self = this;
        self.period = period;
        self.ratingSheetIds = period.sheetsRatingId;
        if (self.faculty.id) {
            self.requestUrl = period_settings_1.PeriodSettings.API_PERIOD_FACULTY_CREATE_RATING_DB(period.id, self.faculty.id);
        }
    };
    PeriodSubjectRatingListComponent.prototype.onLoadFaculty = function (faculty) {
        var self = this;
        self.faculty = faculty;
        if (self.period.id) {
            self.requestUrl = period_settings_1.PeriodSettings.API_PERIOD_FACULTY_CREATE_RATING_DB(self.period.id, faculty.id);
        }
    };
    PeriodSubjectRatingListComponent.prototype.changeActiveGroup = function (group) {
        var self = this;
        self.activeGroup = group;
        self.activeSubject = null;
    };
    PeriodSubjectRatingListComponent.prototype.changeActiveSubject = function (subject) {
        var self = this;
        self.activeSubject = subject;
    };
    return PeriodSubjectRatingListComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], PeriodSubjectRatingListComponent.prototype, "ratingSheetIds", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", period_1.Period)
], PeriodSubjectRatingListComponent.prototype, "period", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", faculty_1.Faculty)
], PeriodSubjectRatingListComponent.prototype, "faculty", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], PeriodSubjectRatingListComponent.prototype, "isPrivate", void 0);
PeriodSubjectRatingListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './period-subject-rating-list.component.html',
        styleUrls: ['./period-subject-rating-list.component.css'],
        selector: 'period-subject-rating-list'
    }),
    __metadata("design:paramtypes", [period_service_1.PeriodService])
], PeriodSubjectRatingListComponent);
exports.PeriodSubjectRatingListComponent = PeriodSubjectRatingListComponent;
//# sourceMappingURL=period-subject-rating-list.component.js.map