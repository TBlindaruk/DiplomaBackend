import {Component, Input, OnInit} from "@angular/core";
import {PeriodService} from "../../../service/period/period.service";
import {Period} from "../../../model/period/period";
import {Faculty} from "../../../model/faculty/faculty";
import {PeriodSettings} from "../../../settings/period.settings";


@Component({
    moduleId: module.id,
    templateUrl: './period-subject-rating-list.component.html',
    styleUrls: ['./period-subject-rating-list.component.css'],
    selector: 'period-subject-rating-list'
})

export class PeriodSubjectRatingListComponent{

    @Input()
    ratingSheetIds: any = null;
    requestUrl: string = null;
    @Input()
    period: Period = new Period();
    @Input()
    faculty: Faculty = new Faculty();

    isActive: boolean = false;

    activeGroup: any = null;
    activeSubject: any = null;

    @Input()
    isPrivate:boolean = true;

    public constructor(private _periodService: PeriodService) {

    }


    createSubjectRatingList() {
        let self = this;
        self.isActive = true;
        self._periodService.createGoogleSheets(self.requestUrl).subscribe(function (responce) {
            self.ratingSheetIds = responce;
            self.isActive = false;
        });
    }

    createResultRatingSheets(){
        let self = this;
        self.isActive = true;
        let url = PeriodSettings.API_PERIOD_FACULTY_CREATE_RESULT_RATING_DB(self.period.id,self.faculty.id);
        self._periodService.createGoogleSheets(url).subscribe(function (responce) {
            self.ratingSheetIds = responce;
            for(let group of self.ratingSheetIds){
                if(self.activeGroup['group_name']=== group['group_name']){
                    self.activeGroup = group;
                    self.activeSubject = null;
                    break;
                }
            }
            self.isActive = false;
        });
    }

    onLoadPeriod(period: Period){
        let self = this;
        self.period = period;
        self.ratingSheetIds = period.sheetsRatingId;
        if(self.faculty.id) {
            self.requestUrl = PeriodSettings.API_PERIOD_FACULTY_CREATE_RATING_DB(period.id,self.faculty.id);
        }
    }

    onLoadFaculty(faculty: Faculty){
        let self = this;
        self.faculty = faculty;
        if(self.period.id) {
            self.requestUrl = PeriodSettings.API_PERIOD_FACULTY_CREATE_RATING_DB(self.period.id,faculty.id);
        }
    }

    changeActiveGroup(group: any){
        let self = this;
        self.activeGroup = group;
        self.activeSubject = null;
    }

    changeActiveSubject(subject:any){
        let self = this;
        self.activeSubject = subject;
    }
}