"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var period_layout_module_1 = require("../period-layout/period-layout.module");
var google_sheets_module_1 = require("../../../core/components/google-sheets/google-sheets.module");
var period_subject_rating_list_component_1 = require("./period-subject-rating-list.component");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var PeriodSubjectRatingListModule = (function () {
    function PeriodSubjectRatingListModule() {
    }
    return PeriodSubjectRatingListModule;
}());
PeriodSubjectRatingListModule = __decorate([
    core_1.NgModule({
        declarations: [
            period_subject_rating_list_component_1.PeriodSubjectRatingListComponent
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            period_layout_module_1.PeriodLayoutModule,
            google_sheets_module_1.GoogleSheetsModule,
            router_1.RouterModule.forChild([
                { path: 'period/:id/faculty/:facultyId/rating', component: period_subject_rating_list_component_1.PeriodSubjectRatingListComponent }
            ]),
            loader_module_1.LoaderModule,
            google_sheets_module_1.GoogleSheetsModule
        ],
        providers: [],
        exports: [
            period_subject_rating_list_component_1.PeriodSubjectRatingListComponent
        ]
    })
], PeriodSubjectRatingListModule);
exports.PeriodSubjectRatingListModule = PeriodSubjectRatingListModule;
//# sourceMappingURL=period-subject-rating-list.module.js.map