import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {PeriodLayoutModule} from "../period-layout/period-layout.module";
import {GoogleSheetsModule} from "../../../core/components/google-sheets/google-sheets.module";
import {PeriodSubjectRatingListComponent} from "./period-subject-rating-list.component";
import {LoaderModule} from "../../../core/components/loader/loader.module";


@NgModule({
    declarations: [
        PeriodSubjectRatingListComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        PeriodLayoutModule,
        GoogleSheetsModule,
        RouterModule.forChild([
            {path: 'period/:id/faculty/:facultyId/rating', component: PeriodSubjectRatingListComponent}
        ]),
        LoaderModule,
        GoogleSheetsModule
    ],
    providers: [/*PeriodService*/],
    exports:[
        PeriodSubjectRatingListComponent
    ]
})
export class PeriodSubjectRatingListModule {

}
