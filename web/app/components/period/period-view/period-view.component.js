"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var faculty_1 = require("../../../model/faculty/faculty");
var faculty_service_1 = require("../../../service/faculty/faculty.service");
var PeriodViewComponent = (function () {
    function PeriodViewComponent(_facultyService) {
        this._facultyService = _facultyService;
        this.faculties = [];
        this.facultyId = 0;
        this.isActive = true;
    }
    PeriodViewComponent.prototype.ngOnInit = function () {
        var self = this;
        this._facultyService.getFaculties().subscribe(function (faculties) {
            for (var _i = 0, faculties_1 = faculties; _i < faculties_1.length; _i++) {
                var faculty = faculties_1[_i];
                self.faculties.push(new faculty_1.Faculty(faculty));
                if (!self.facultyId) {
                    self.facultyId = faculty.id;
                }
            }
            self.isActive = false;
        });
    };
    return PeriodViewComponent;
}());
PeriodViewComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './period-view.component.html',
        styleUrls: ['./period-view.component.css']
    }),
    __metadata("design:paramtypes", [faculty_service_1.FacultyService])
], PeriodViewComponent);
exports.PeriodViewComponent = PeriodViewComponent;
//# sourceMappingURL=period-view.component.js.map