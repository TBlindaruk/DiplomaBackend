import {Component, OnInit} from "@angular/core";
import {Faculty} from "../../../model/faculty/faculty";
import {FacultyService} from "../../../service/faculty/faculty.service";


@Component({
    moduleId: module.id,
    templateUrl: './period-view.component.html',
    styleUrls: ['./period-view.component.css']
})

export class PeriodViewComponent implements OnInit{

    faculties: Faculty[] = [];
    facultyId: number = 0;
    isActive: boolean = true;

    constructor(private _facultyService: FacultyService) {

    }

    ngOnInit(): void {
        let self = this;
        this._facultyService.getFaculties().subscribe(function (faculties) {
            for (let faculty of faculties) {
                self.faculties.push(new Faculty(faculty));
                if(!self.facultyId){
                    self.facultyId = faculty.id;
                }
            }
            self.isActive = false;
        });
    }

}