"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var period_view_component_1 = require("./period-view.component");
var period_layout_module_1 = require("../period-layout/period-layout.module");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var PeriodViewModule = (function () {
    function PeriodViewModule() {
    }
    return PeriodViewModule;
}());
PeriodViewModule = __decorate([
    core_1.NgModule({
        declarations: [
            period_view_component_1.PeriodViewComponent
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            period_layout_module_1.PeriodLayoutModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            router_1.RouterModule.forChild([
                { path: 'period/:id', component: period_view_component_1.PeriodViewComponent },
            ]),
            loader_module_1.LoaderModule
        ],
        providers: []
    })
], PeriodViewModule);
exports.PeriodViewModule = PeriodViewModule;
//# sourceMappingURL=period-view.module.js.map