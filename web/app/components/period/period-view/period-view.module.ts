import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {PeriodViewComponent} from "./period-view.component";
import {PeriodLayoutModule} from "../period-layout/period-layout.module";
import {LoaderModule} from "../../../core/components/loader/loader.module";

@NgModule({
    declarations: [
        PeriodViewComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        PeriodLayoutModule,
        ModalModule.forRoot(),
        RouterModule.forChild([
            {path: 'period/:id', component: PeriodViewComponent},
        ]),
        LoaderModule
    ],
    providers: [/*PeriodService*/]
})
export class PeriodViewModule {

}
