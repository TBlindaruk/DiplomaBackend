"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var period_view_module_1 = require("./period-view/period-view.module");
var period_list_module_1 = require("./period-list/period-list.module");
var period_service_1 = require("../../service/period/period.service");
var period_layout_module_1 = require("./period-layout/period-layout.module");
var period_faculty_db_module_1 = require("./period-faculty-db/period-faculty-db.module");
var period_subject_rating_list_module_1 = require("./period-subject-rating-list/period-subject-rating-list.module");
var period_scholarship_module_1 = require("./period-scholarship/period-scholarship.module");
var PeriodModule = (function () {
    function PeriodModule() {
    }
    return PeriodModule;
}());
PeriodModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            period_layout_module_1.PeriodLayoutModule,
            period_view_module_1.PeriodViewModule,
            period_list_module_1.PeriodListModule,
            period_faculty_db_module_1.PeriodFacultyDbModule,
            period_subject_rating_list_module_1.PeriodSubjectRatingListModule,
            period_scholarship_module_1.PeriodScholarshipModule
        ],
        providers: [period_service_1.PeriodService]
    })
], PeriodModule);
exports.PeriodModule = PeriodModule;
//# sourceMappingURL=period.module.js.map