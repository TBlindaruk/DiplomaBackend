import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {PeriodViewModule} from "./period-view/period-view.module";
import {PeriodListModule} from "./period-list/period-list.module";
import {PeriodService} from "../../service/period/period.service";
import {PeriodLayoutModule} from "./period-layout/period-layout.module";
import {PeriodFacultyDbModule} from "./period-faculty-db/period-faculty-db.module";
import {PeriodSubjectRatingListModule} from "./period-subject-rating-list/period-subject-rating-list.module";
import {PeriodScholarshipModule} from "./period-scholarship/period-scholarship.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        PeriodLayoutModule,
        PeriodViewModule,
        PeriodListModule,
        PeriodFacultyDbModule,
        PeriodSubjectRatingListModule,
        PeriodScholarshipModule
    ],
    providers: [PeriodService]
})
export class PeriodModule {

}
