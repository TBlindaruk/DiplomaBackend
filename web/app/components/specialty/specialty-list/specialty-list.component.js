"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var faculty_1 = require("../../../model/faculty/faculty");
var specialty_1 = require("../../../model/specialty/specialty");
var faculty_service_1 = require("../../../service/faculty/faculty.service");
var specialty_service_1 = require("../../../service/specialty/specialty.service");
var SpecialtyListComponent = (function () {
    function SpecialtyListComponent(_facultyService, _specialtyService) {
        this._facultyService = _facultyService;
        this._specialtyService = _specialtyService;
        this.faculties = [];
        this.errorMessage = '';
        this.specialties = [];
        this.specialty = new specialty_1.Specialty();
        this.isActive = true;
    }
    SpecialtyListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this._facultyService.getFaculties().subscribe(function (faculties) {
            for (var facultyIndex in faculties) {
                self.faculties.push(new faculty_1.Faculty(faculties[facultyIndex]));
            }
            if (self.specialties) {
                self.isActive = false;
            }
        }, function (error) { return _this.errorMessage = error; });
        this._specialtyService.getSpecialties().subscribe(function (specialties) {
            for (var specialtyIndex in specialties) {
                self.specialties.push(new specialty_1.Specialty(specialties[specialtyIndex]));
            }
            if (self.faculties) {
                self.isActive = false;
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    SpecialtyListComponent.prototype.createSpecialty = function () {
        var self = this;
        self.isActive = true;
        self._specialtyService.addSpecialty(self.specialty).subscribe(function (specialty) {
            self.createSpecialtyModal.hide();
            var faculty = self.findFaculty(self.specialty.faculty.id);
            if (faculty) {
                self.specialty.faculty = faculty;
            }
            self.specialties.unshift(new specialty_1.Specialty(specialty));
            self.specialty.clear();
            self.isActive = false;
        });
    };
    SpecialtyListComponent.prototype.onDeleteSpecialty = function (event) {
        for (var i = 0; i < this.specialties.length; ++i) {
            if (event.id == this.specialties[i].id) {
                this.specialties.splice(i, 1);
                break;
            }
        }
    };
    SpecialtyListComponent.prototype.findFaculty = function (facultyId) {
        for (var _i = 0, _a = this.faculties; _i < _a.length; _i++) {
            var faculty = _a[_i];
            if (faculty.id == facultyId) {
                return faculty;
            }
        }
        return null;
    };
    return SpecialtyListComponent;
}());
__decorate([
    core_1.ViewChild('createSpecialtyModal'),
    __metadata("design:type", ng2_bootstrap_1.ModalDirective)
], SpecialtyListComponent.prototype, "createSpecialtyModal", void 0);
SpecialtyListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './specialty-list.component.html',
        styleUrls: ['./specialty-list.component.css'],
        selector: 'specialty-list'
    }),
    __metadata("design:paramtypes", [faculty_service_1.FacultyService,
        specialty_service_1.SpecialityService])
], SpecialtyListComponent);
exports.SpecialtyListComponent = SpecialtyListComponent;
//# sourceMappingURL=specialty-list.component.js.map