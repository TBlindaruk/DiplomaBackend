import {Component, OnInit, ViewChild} from "@angular/core";

import {ModalDirective} from "ng2-bootstrap";
import {Faculty} from "../../../model/faculty/faculty";
import {Specialty} from "../../../model/specialty/specialty";
import {FacultyService} from "../../../service/faculty/faculty.service";
import {SpecialityService} from "../../../service/specialty/specialty.service";

@Component({
    moduleId: module.id,
    templateUrl: './specialty-list.component.html',
    styleUrls: ['./specialty-list.component.css'],
    selector: 'specialty-list'
})

export class SpecialtyListComponent implements OnInit{

    faculties: Faculty[] = [];
    errorMessage: string ='';
    specialties: Specialty[] = [];
    specialty: Specialty = new Specialty();
    isActive: boolean = true;

    @ViewChild('createSpecialtyModal') createSpecialtyModal: ModalDirective;

    constructor(
        private _facultyService: FacultyService,
        private _specialtyService: SpecialityService
    ){

    }

    ngOnInit(): void {
        let self = this;
        this._facultyService.getFaculties().subscribe(function(faculties){
            for(let facultyIndex in faculties){
                self.faculties.push(new Faculty(faculties[facultyIndex]));
            }
            if(self.specialties){
                self.isActive = false;
            }
        }, error => this.errorMessage = <any> error);

        this._specialtyService.getSpecialties().subscribe(function (specialties) {
            for(let specialtyIndex in specialties){
                self.specialties.push(new Specialty(specialties[specialtyIndex]));
            }
            if(self.faculties){
                self.isActive = false;
            }
        }, error => this.errorMessage = <any> error);
    }

    createSpecialty(): void{
        let self = this;
        self.isActive = true;
        self._specialtyService.addSpecialty(self.specialty).subscribe(function(specialty){
            self.createSpecialtyModal.hide();
            let faculty = self.findFaculty(self.specialty.faculty.id);
            if(faculty){
                self.specialty.faculty = faculty;
            }
            self.specialties.unshift(new Specialty(specialty));
            self.specialty.clear();
            self.isActive = false;
        });
    }

    onDeleteSpecialty(event: Specialty){
        for (let i = 0; i < this.specialties.length; ++i) {
            if (event.id == this.specialties[i].id) {
                this.specialties.splice(i, 1);
                break;
            }
        }
    }

    findFaculty(facultyId: number | string):Faculty{
        for(let faculty of this.faculties){
            if(faculty.id == facultyId){
                return faculty;
            }
        }
        return null;
    }
}