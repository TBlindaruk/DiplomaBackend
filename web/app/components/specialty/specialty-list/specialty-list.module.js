"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var specialty_list_component_1 = require("./specialty-list.component");
var specialty_service_1 = require("../../../service/specialty/specialty.service");
var faculty_service_1 = require("../../../service/faculty/faculty.service");
var specialty_row_module_1 = require("../specialty-row/specialty-row.module");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var SpecialtyListModule = (function () {
    function SpecialtyListModule() {
    }
    return SpecialtyListModule;
}());
SpecialtyListModule = __decorate([
    core_1.NgModule({
        declarations: [
            specialty_list_component_1.SpecialtyListComponent
        ],
        providers: [
            specialty_service_1.SpecialityService,
            faculty_service_1.FacultyService
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            specialty_row_module_1.SpecialtyRowModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            loader_module_1.LoaderModule,
            router_1.RouterModule.forChild([
                { path: 'specialty', component: specialty_list_component_1.SpecialtyListComponent },
            ])
        ],
    })
], SpecialtyListModule);
exports.SpecialtyListModule = SpecialtyListModule;
//# sourceMappingURL=specialty-list.module.js.map