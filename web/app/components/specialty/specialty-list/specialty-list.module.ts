import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {SpecialtyListComponent} from "./specialty-list.component";
import {SpecialityService} from "../../../service/specialty/specialty.service";
import {FacultyService} from "../../../service/faculty/faculty.service";
import {SpecialtyRowModule} from "../specialty-row/specialty-row.module";
import {LoaderModule} from "../../../core/components/loader/loader.module";

@NgModule({
    declarations: [
        SpecialtyListComponent
    ],
    providers: [
        SpecialityService,
        FacultyService
    ],

    imports: [
        CommonModule,
        FormsModule,
        SpecialtyRowModule,
        ModalModule.forRoot(),
        LoaderModule,
        RouterModule.forChild([
            {path: 'specialty', component: SpecialtyListComponent},
        ])
    ],
})
export class SpecialtyListModule {

}
