"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var specialty_1 = require("../../../model/specialty/specialty");
var specialty_service_1 = require("../../../service/specialty/specialty.service");
var SpecialtyRowComponent = (function () {
    function SpecialtyRowComponent(_specialtyService) {
        this._specialtyService = _specialtyService;
        this.deleteSpecialty = new core_1.EventEmitter();
        this.editMode = false;
        this.isActive = false;
    }
    SpecialtyRowComponent.prototype.changeMode = function () {
        this.editMode = !this.editMode;
    };
    SpecialtyRowComponent.prototype.edit = function () {
        var self = this;
        var specialty = this.specialty;
        self.isActive = true;
        this._specialtyService.editSpecialty(specialty).subscribe(function (response) {
            self.changeMode();
            self.specialty = new specialty_1.Specialty(response);
            self.isActive = false;
        });
    };
    SpecialtyRowComponent.prototype.onDeleteFaculty = function () {
        var self = this;
        self.isActive = true;
        this._specialtyService.deleteSpecialty(self.specialty).subscribe(function () {
            self.deleteSpecialty.emit(self.specialty);
            self.isActive = false;
        });
    };
    return SpecialtyRowComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", specialty_1.Specialty)
], SpecialtyRowComponent.prototype, "specialty", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], SpecialtyRowComponent.prototype, "faculties", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], SpecialtyRowComponent.prototype, "deleteSpecialty", void 0);
SpecialtyRowComponent = __decorate([
    core_1.Component({
        selector: '.specialty-row',
        moduleId: module.id,
        templateUrl: './specialty-row.component.html',
        styleUrls: ['./specialty-row.component.css']
    }),
    __metadata("design:paramtypes", [specialty_service_1.SpecialityService])
], SpecialtyRowComponent);
exports.SpecialtyRowComponent = SpecialtyRowComponent;
//# sourceMappingURL=specialty-row.component.js.map