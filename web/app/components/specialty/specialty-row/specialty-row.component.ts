
import {Component, Input, Output, OnInit, ViewChild, EventEmitter} from "@angular/core";
import {Specialty} from "../../../model/specialty/specialty";
import {SpecialityService} from "../../../service/specialty/specialty.service";
import {Faculty} from "../../../model/faculty/faculty";

@Component({
    selector: '.specialty-row',
    moduleId: module.id,
    templateUrl: './specialty-row.component.html',
    styleUrls: ['./specialty-row.component.css']
})

export class SpecialtyRowComponent{

    @Input()
    specialty: Specialty;

    @Input()
    faculties: Faculty[];

    @Output()
    deleteSpecialty: EventEmitter<Specialty> = new EventEmitter<Specialty>();

    editMode: boolean = false;
    isActive: boolean = false;

    constructor(private _specialtyService: SpecialityService){

    }

    changeMode(): void{
        this.editMode = !this.editMode;
    }

    edit():void {
        let self = this;
        let specialty = this.specialty;
        self.isActive = true;
        this._specialtyService.editSpecialty(specialty).subscribe(function (response) {
            self.changeMode();
            self.specialty = new Specialty(response);
            self.isActive = false;
        });
    }

    onDeleteFaculty() {
        let self = this;
        self.isActive = true;
        this._specialtyService.deleteSpecialty(self.specialty).subscribe(function () {
            self.deleteSpecialty.emit(self.specialty);
            self.isActive = false;
        });
    }
}