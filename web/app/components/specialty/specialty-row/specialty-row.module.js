"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var specialty_row_component_1 = require("./specialty-row.component");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var SpecialtyRowModule = (function () {
    function SpecialtyRowModule() {
    }
    return SpecialtyRowModule;
}());
SpecialtyRowModule = __decorate([
    core_1.NgModule({
        declarations: [
            specialty_row_component_1.SpecialtyRowComponent
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            loader_module_1.LoaderModule
        ],
        exports: [
            specialty_row_component_1.SpecialtyRowComponent
        ]
    })
], SpecialtyRowModule);
exports.SpecialtyRowModule = SpecialtyRowModule;
//# sourceMappingURL=specialty-row.module.js.map