import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {SpecialtyRowComponent} from "./specialty-row.component";
import {LoaderModule} from "../../../core/components/loader/loader.module";

@NgModule({
    declarations: [
        SpecialtyRowComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoaderModule
    ],
    exports:[
        SpecialtyRowComponent
    ]
})
export class SpecialtyRowModule {

}
