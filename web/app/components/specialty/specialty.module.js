"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// import {RouterModule} from '@angular/router';
// import {PeriodService} from "./model/period.service";
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
// import {FacultyListModule} from "./view/faculty-list/faculty-list.module";
var ng2_bootstrap_1 = require("ng2-bootstrap");
var specialty_list_module_1 = require("./specialty-list/specialty-list.module");
var specialty_service_1 = require("../../service/specialty/specialty.service");
var faculty_service_1 = require("../../service/faculty/faculty.service");
var SpecialtyModule = (function () {
    function SpecialtyModule() {
    }
    return SpecialtyModule;
}());
SpecialtyModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            specialty_list_module_1.SpecialtyListModule
            // PeriodViewModule,
            // PeriodListModule
        ],
        providers: [
            specialty_service_1.SpecialityService,
            faculty_service_1.FacultyService
        ]
    })
], SpecialtyModule);
exports.SpecialtyModule = SpecialtyModule;
//# sourceMappingURL=specialty.module.js.map