import {NgModule} from '@angular/core';
// import {RouterModule} from '@angular/router';

// import {PeriodService} from "./model/period.service";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
// import {FacultyListModule} from "./view/faculty-list/faculty-list.module";
import {ModalModule} from "ng2-bootstrap";
import {SpecialtyListModule} from "./specialty-list/specialty-list.module";
import {SpecialityService} from "../../service/specialty/specialty.service";
import {FacultyService} from "../../service/faculty/faculty.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        SpecialtyListModule

        // PeriodViewModule,
        // PeriodListModule

    ],
    providers: [
        SpecialityService,
        FacultyService
    ]
})
export class SpecialtyModule {

}
