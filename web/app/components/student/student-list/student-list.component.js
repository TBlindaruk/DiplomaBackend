"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var student_1 = require("../../../model/student/student");
var group_1 = require("../../../model/group/group");
var period_1 = require("../../../model/period/period");
var student_service_1 = require("../../../service/student/student.service");
var group_service_1 = require("../../../service/group/group.service");
var period_service_1 = require("../../../service/period/period.service");
var StudentListComponent = (function () {
    function StudentListComponent(_studentService, _groupService, _periodService) {
        this._studentService = _studentService;
        this._groupService = _groupService;
        this._periodService = _periodService;
        this.students = [];
        this.studentsRoot = [];
        this.student = new student_1.Student();
        this.periods = [];
        this.groups = [];
        this.isActive = true;
        this.ratingPaternValidation = "";
    }
    StudentListComponent.prototype.ngOnInit = function () {
        var self = this;
        self.getStudents();
        this._groupService.getGroups().subscribe(function (groups) {
            for (var group in groups) {
                self.groups.push(new group_1.Group(groups[group]));
            }
        });
        this._periodService.getPeriods().subscribe(function (periods) {
            for (var period in periods) {
                self.periods.push(new period_1.Period(periods[period]));
            }
        });
    };
    StudentListComponent.prototype.getStudents = function () {
        var self = this;
        self.studentsRoot = [];
        this._studentService.getStudents().subscribe(function (students) {
            self.students = [];
            for (var student in students) {
                var newStudent = new student_1.Student(students[student]);
                if (newStudent.isRoot) {
                    self.studentsRoot.push(newStudent);
                }
                self.students.push(newStudent);
            }
            var childrenIds = [];
            for (var i = 0; i < self.students.length; ++i) {
                var student = self.students[i];
                for (var j = 0; j < student.children.length; ++j) {
                    var id = student.children[j].id;
                    childrenIds[id] = id;
                }
            }
            for (var i in self.students) {
                var id = self.students[i].id;
                if (childrenIds[id]) {
                    self.students[i].isShow = false;
                }
            }
            self.isActive = false;
        });
    };
    StudentListComponent.prototype.createStudent = function ($forms) {
        var self = this;
        self.isActive = true;
        self._studentService.addStudent(self.student).subscribe(function (student) {
            var newStudent = new student_1.Student(student);
            self._addChild(newStudent);
            if (newStudent.isRoot) {
                self.studentsRoot.unshift(newStudent);
            }
            self.createStudentModal.hide();
            self.students.unshift(newStudent);
            self.student.clear();
            self.isActive = false;
        });
    };
    /**
     *
     * @param newStudent
     * @private
     */
    StudentListComponent.prototype._addChild = function (newStudent) {
        var self = this;
        var id = self.student.parent.id;
        if (id) {
            for (var i = 0; i < self.students.length; ++i) {
                if (id == self.students[i].id) {
                    newStudent.isShow = false;
                    self.students[i].addChildren(newStudent);
                }
            }
        }
    };
    StudentListComponent.prototype.onDeleteStudent = function (event) {
        var self = this;
        self.isActive = true;
        self.getStudents();
    };
    StudentListComponent.prototype.onEditStudent = function (event) {
        if (!event.isRoot) {
            var self_1 = this;
            self_1.isActive = true;
            self_1.getStudents();
        }
    };
    StudentListComponent.prototype.isRootStudent = function (student) {
        return !(student.isRoot == false || student.isRoot == '0' || student.isRoot == '');
    };
    return StudentListComponent;
}());
__decorate([
    core_1.ViewChild('createStudentModal'),
    __metadata("design:type", ng2_bootstrap_1.ModalDirective)
], StudentListComponent.prototype, "createStudentModal", void 0);
StudentListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './student-list.component.html',
        styleUrls: ['./student-list.component.css'],
        selector: 'student-list'
    }),
    __metadata("design:paramtypes", [student_service_1.StudentService,
        group_service_1.GroupService,
        period_service_1.PeriodService])
], StudentListComponent);
exports.StudentListComponent = StudentListComponent;
//# sourceMappingURL=student-list.component.js.map