import {Component, OnInit, ViewChild} from "@angular/core";

import {ModalDirective} from "ng2-bootstrap";

import {Student} from "../../../model/student/student";
import {Group} from "../../../model/group/group";
import {Period} from "../../../model/period/period";
import {StudentService} from "../../../service/student/student.service";
import {GroupService} from "../../../service/group/group.service";
import {PeriodService} from "../../../service/period/period.service";

@Component({
    moduleId: module.id,
    templateUrl: './student-list.component.html',
    styleUrls: ['./student-list.component.css'],
    selector: 'student-list'
})

export class StudentListComponent implements OnInit {

    students: Student[] = [];
    studentsRoot: Student[] = [];
    student: Student = new Student();
    periods: Period[] = [];
    groups: Group[] = [];
    isActive: boolean = true;
    ratingPaternValidation: string = "";
    @ViewChild('createStudentModal') createStudentModal: ModalDirective;

    constructor(private _studentService: StudentService,
                private _groupService: GroupService,
                private _periodService: PeriodService) {

    }

    ngOnInit(): void {
        let self = this;
        self.getStudents();
        this._groupService.getGroups().subscribe(function (groups) {
            for(let group in groups){
                self.groups.push(new Group(groups[group]));
            }
        });
        this._periodService.getPeriods().subscribe(function (periods) {
            for(let period in periods){
                self.periods.push(new Period(periods[period]));
            }
        });
    }

    public getStudents(){
        let self = this;
        self.studentsRoot = [];
        this._studentService.getStudents().subscribe(function (students) {
            self.students = [];
            for(let student in students){
                let newStudent = new Student(students[student]);
                if (newStudent.isRoot) {
                    self.studentsRoot.push(newStudent);
                }
                self.students.push(newStudent);
            }

            let childrenIds: number[] = [];
            for (let i = 0; i < self.students.length; ++i) {
                let student = self.students[i];
                for (let j = 0; j < student.children.length; ++j) {
                    let id = student.children[j].id;
                    childrenIds[id] = id;
                }
            }

            for (let i in self.students) {
                let id = self.students[i].id;
                if (childrenIds[id]) {
                    self.students[i].isShow = false;
                }
            }
            self.isActive = false;
        });
    }

    createStudent($forms:any) {
        let self = this;
        self.isActive = true;
        self._studentService.addStudent(self.student).subscribe(function (student) {
            let newStudent = new Student(student);
            self._addChild(newStudent);
            if(newStudent.isRoot){
                self.studentsRoot.unshift(newStudent);
            }
            self.createStudentModal.hide();
            self.students.unshift(newStudent);
            self.student.clear();
            self.isActive = false;
        });
    }

    /**
     *
     * @param newStudent
     * @private
     */
    protected _addChild(newStudent: Student){
        let self = this;
        let id = self.student.parent.id;
        if(id){
            for(let i = 0; i<self.students.length;++i){
                if(id == self.students[i].id){
                    newStudent.isShow = false;
                    self.students[i].addChildren(newStudent);
                }
            }
        }
    }

    onDeleteStudent(event: Student) {
        let self = this;
        self.isActive = true;
        self.getStudents();
    }

    onEditStudent(event: Student){
        if(!event.isRoot) {
            let self = this;
            self.isActive = true;
            self.getStudents();
        }
    }

    isRootStudent(student: any) {
        return !(student.isRoot == false || student.isRoot == '0' || student.isRoot == '');
    }
}