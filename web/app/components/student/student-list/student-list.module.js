"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var loader_module_1 = require("../../../core/components/loader/loader.module");
var student_list_component_1 = require("./student-list.component");
var student_service_1 = require("../../../service/student/student.service");
var student_row_module_1 = require("../student-row/student-row.module");
var StudentListModule = (function () {
    function StudentListModule() {
    }
    return StudentListModule;
}());
StudentListModule = __decorate([
    core_1.NgModule({
        declarations: [
            student_list_component_1.StudentListComponent
        ],
        providers: [
            student_service_1.StudentService,
        ],
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ng2_bootstrap_1.ModalModule.forRoot(),
            loader_module_1.LoaderModule,
            router_1.RouterModule.forChild([
                { path: 'student', component: student_list_component_1.StudentListComponent },
            ]),
            student_row_module_1.StudentRowModule
        ],
    })
], StudentListModule);
exports.StudentListModule = StudentListModule;
//# sourceMappingURL=student-list.module.js.map