import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {LoaderModule} from "../../../core/components/loader/loader.module";
import {StudentListComponent} from "./student-list.component";
import {StudentService} from "../../../service/student/student.service";
import {StudentRowModule} from "../student-row/student-row.module";

@NgModule({
    declarations: [
        StudentListComponent
    ],
    providers: [
        StudentService,
    ],

    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        LoaderModule,
        RouterModule.forChild([
            {path: 'student', component: StudentListComponent},
        ]),
        StudentRowModule
    ],
})
export class StudentListModule {

}
