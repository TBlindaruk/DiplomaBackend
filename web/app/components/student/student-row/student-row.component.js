"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var student_1 = require("../../../model/student/student");
var student_service_1 = require("../../../service/student/student.service");
var StudentRowComponent = (function () {
    function StudentRowComponent(_studentService) {
        this._studentService = _studentService;
        this.isChild = false;
        this.studentsRoot = [];
        this.deleteStudent = new core_1.EventEmitter();
        this.editStudent = new core_1.EventEmitter();
        this.editMode = false;
        this.isActive = false;
    }
    StudentRowComponent.prototype.changeMode = function () {
        this.editMode = !this.editMode;
    };
    StudentRowComponent.prototype.edit = function () {
        var self = this;
        var student = this.student;
        self.isActive = true;
        this._studentService.editStudent(student).subscribe(function (response) {
            self.changeMode();
            self.student = new student_1.Student(response);
            self.editStudent.emit(new student_1.Student(response));
            self.isActive = false;
        });
    };
    StudentRowComponent.prototype.onDeleteStudent = function () {
        var self = this;
        self.isActive = true;
        this._studentService.deleteStudent(self.student).subscribe(function () {
            self.deleteStudent.emit(self.student);
            self.isActive = false;
        });
    };
    StudentRowComponent.prototype.isRootStudent = function (student) {
        return !(student.isRoot == false || student.isRoot == '0' || student.isRoot == '');
    };
    return StudentRowComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", student_1.Student)
], StudentRowComponent.prototype, "student", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], StudentRowComponent.prototype, "isChild", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], StudentRowComponent.prototype, "studentsRoot", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], StudentRowComponent.prototype, "deleteStudent", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], StudentRowComponent.prototype, "editStudent", void 0);
StudentRowComponent = __decorate([
    core_1.Component({
        selector: '.student-row',
        moduleId: module.id,
        templateUrl: './student-row.component.html',
        styleUrls: ['./student-row.component.css']
    }),
    __metadata("design:paramtypes", [student_service_1.StudentService])
], StudentRowComponent);
exports.StudentRowComponent = StudentRowComponent;
//# sourceMappingURL=student-row.component.js.map