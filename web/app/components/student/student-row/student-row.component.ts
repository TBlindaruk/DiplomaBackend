import {Component, Input, Output, OnInit, ViewChild, EventEmitter} from "@angular/core";
import {Student} from "../../../model/student/student";
import {StudentService} from "../../../service/student/student.service";

@Component({
    selector: '.student-row',
    moduleId: module.id,
    templateUrl: './student-row.component.html',
    styleUrls: ['./student-row.component.css']
})

export class StudentRowComponent {

    @Input()
    student: Student;

    @Input()
    isChild: boolean = false;

    @Input()
    studentsRoot: Student[] = [];

    @Output()
    deleteStudent: EventEmitter<Student> = new EventEmitter<Student>();
    @Output()
    editStudent: EventEmitter<Student> = new EventEmitter<Student>();

    editMode: boolean = false;
    isActive: boolean = false;

    constructor(private _studentService: StudentService) {

    }

    changeMode(): void {
        this.editMode = !this.editMode;
    }

    edit(): void {
        let self = this;
        let student = this.student;
        self.isActive = true;
        this._studentService.editStudent(student).subscribe(function (response) {
            self.changeMode();
            self.student = new Student(response);
            self.editStudent.emit(new Student(response));
            self.isActive = false;
        });
    }

    onDeleteStudent() {
        let self = this;
        self.isActive = true;
        this._studentService.deleteStudent(self.student).subscribe(function () {
            self.deleteStudent.emit(self.student);
            self.isActive = false;
        });
    }

    isRootStudent(student: any) {
        return !(student.isRoot == false || student.isRoot == '0' || student.isRoot == '');
    }
}