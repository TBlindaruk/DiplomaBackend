import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {LoaderModule} from "../../../core/components/loader/loader.module";
import {StudentRowComponent} from "./student-row.component";

@NgModule({
    declarations: [
        StudentRowComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoaderModule
    ],
    exports:[
        StudentRowComponent
    ]
})
export class StudentRowModule {

}
