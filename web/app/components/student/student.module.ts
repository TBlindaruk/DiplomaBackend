/**
 * Created by tetiana on 01.06.17.
 */

import {NgModule} from '@angular/core';

import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ModalModule} from "ng2-bootstrap";
import {StudentListModule} from "./student-list/student-list.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        StudentListModule

        // PeriodViewModule,
        // PeriodListModule

    ],
    providers: [

    ]
})
export class StudentModule {

}
