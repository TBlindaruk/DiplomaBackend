"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var period_service_1 = require("../../../service/period/period.service");
var GoogleSheetsComponent = (function () {
    function GoogleSheetsComponent(_periodService) {
        this._periodService = _periodService;
        this.googleSheets = null;
        this.requestUrl = null;
        this.createSheetsButtonName = null;
        this.editMode = true;
        this.showButton = false;
        this.isActive = false;
    }
    GoogleSheetsComponent.prototype.createGroupsSheets = function () {
        var self = this;
        self.isActive = true;
        self._periodService.createGoogleSheets(self.requestUrl).subscribe(function (responce) {
            if (responce) {
                self.googleSheets = responce.sheetId;
            }
            self.isActive = false;
        });
    };
    return GoogleSheetsComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], GoogleSheetsComponent.prototype, "googleSheets", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], GoogleSheetsComponent.prototype, "requestUrl", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], GoogleSheetsComponent.prototype, "createSheetsButtonName", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], GoogleSheetsComponent.prototype, "editMode", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], GoogleSheetsComponent.prototype, "showButton", void 0);
GoogleSheetsComponent = __decorate([
    core_1.Component({
        selector: 'google-sheets',
        moduleId: module.id,
        templateUrl: './google-sheets.component.html',
        styleUrls: ['./google-sheets.component.css']
    }),
    __metadata("design:paramtypes", [period_service_1.PeriodService])
], GoogleSheetsComponent);
exports.GoogleSheetsComponent = GoogleSheetsComponent;
//# sourceMappingURL=google-sheets.component.js.map