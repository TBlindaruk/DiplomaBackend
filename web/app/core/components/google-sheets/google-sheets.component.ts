import {Component, Input, Output, OnInit, ViewChild, EventEmitter} from "@angular/core";
import {PeriodService} from "../../../service/period/period.service";

@Component({
    selector: 'google-sheets',
    moduleId: module.id,
    templateUrl: './google-sheets.component.html',
    styleUrls: ['./google-sheets.component.css']
})

export class GoogleSheetsComponent {

    @Input() googleSheets: string = null;
    @Input() requestUrl: string = null;
    @Input() createSheetsButtonName: string = null;
    @Input() editMode: Boolean = true;
    @Input() showButton:boolean = false;

    isActive: boolean = false;

    public constructor(private _periodService: PeriodService) {

    }

    createGroupsSheets() {
        let self = this;
        self.isActive = true;
        self._periodService.createGoogleSheets(self.requestUrl).subscribe(function (responce) {
            if(responce) {
                self.googleSheets = responce.sheetId;
            }
            self.isActive = false;
        });
    }
}