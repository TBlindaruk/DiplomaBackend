import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {SafePipe} from "./safe.pipe";
import {GoogleSheetsComponent} from "./google-sheets.component";
import {LoaderModule} from "../loader/loader.module";

@NgModule({
    declarations: [
        GoogleSheetsComponent,
        SafePipe
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoaderModule
    ],
    exports:[
        GoogleSheetsComponent
    ]
})
export class GoogleSheetsModule {

}
