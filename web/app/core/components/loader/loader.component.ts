import {Component, Input} from "@angular/core";


@Component({
    selector: 'loader',
    moduleId: module.id,
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.css']
})

export class LoaderComponent {
    @Input() active: boolean = true;


}