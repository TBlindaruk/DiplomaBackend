export interface FacultyInterface {
    id?: number;
    name: string;
    shortName: string;
}