"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Faculty = (function () {
    function Faculty(params) {
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.shortName) {
            this.shortName = params.shortName;
        }
        if (params.hasOwnProperty('short_name')) {
            this.shortName = params['short_name'];
        }
    }
    Object.defineProperty(Faculty.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Faculty.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Faculty.prototype, "shortName", {
        get: function () {
            return this._shortName;
        },
        set: function (value) {
            this._shortName = value;
        },
        enumerable: true,
        configurable: true
    });
    Faculty.prototype.clear = function () {
        this.id = null;
        this.name = null;
        this.shortName = null;
    };
    return Faculty;
}());
exports.Faculty = Faculty;
//# sourceMappingURL=faculty.js.map