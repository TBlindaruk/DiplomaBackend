import {FacultyInterface} from "./faculty.interface";
export class Faculty implements FacultyInterface {

    private _id: number;
    private _name: string;
    private _shortName: string;

    constructor(params?: FacultyInterface) {
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.shortName) {
            this.shortName = params.shortName;
        }
        if (params.hasOwnProperty('short_name')) {
            this.shortName = params['short_name'];
        }
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get shortName(): string {
        return this._shortName;
    }

    set shortName(value: string) {
        this._shortName = value;
    }

    clear(): void {
        this.id = null;
        this.name = null;
        this.shortName = null;
    }
}