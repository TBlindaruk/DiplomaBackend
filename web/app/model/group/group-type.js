"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GroupType = (function () {
    function GroupType() {
    }
    GroupType.toOptionArray = function () {
        var self = this;
        return [
            {
                'name': 'MS',
                'value': self.MS
            },
            {
                'name': 'BS',
                'value': self.BS
            }
        ];
    };
    /**
     *
     * @param value
     * @returns {any}
     */
    GroupType.findNameByValue = function (value) {
        if (value == this.BS) {
            return 'BS';
        }
        if (value == this.MS) {
            return 'MS';
        }
        return null;
    };
    return GroupType;
}());
GroupType.MS = 1;
GroupType.BS = 2;
exports.GroupType = GroupType;
//# sourceMappingURL=group-type.js.map