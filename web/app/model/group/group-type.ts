export class GroupType {
    static MS = 1;
    static BS = 2;

    static toOptionArray() {
        let self = this;
        return [
            {
                'name': 'MS',
                'value': self.MS
            },
            {
                'name': 'BS',
                'value': self.BS
            }
        ]
    }

    /**
     *
     * @param value
     * @returns {any}
     */
    static findNameByValue(value: number) {
        if (value == this.BS) {
            return 'BS'
        }
        if (value == this.MS) {
            return 'MS';
        }
        return null;
    }
}