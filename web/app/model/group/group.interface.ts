import {Specialty} from "../specialty/specialty";
export interface GroupInterface {
    id: number;
    specialty?: Specialty;
    name: string;
    groupType?: any;
    startYear?: number;
}