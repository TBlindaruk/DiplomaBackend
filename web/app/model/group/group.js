"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var specialty_1 = require("../specialty/specialty");
var Group = (function () {
    /**
     *
     * @param params
     */
    function Group(params) {
        this._specialty = new specialty_1.Specialty();
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.startYear) {
            this.startYear = params.startYear;
        }
        if (params.hasOwnProperty('start_year')) {
            this.startYear = params['start_year'];
        }
        if (params.groupType) {
            this.groupType = params.groupType;
        }
        if (params.hasOwnProperty('type')) {
            this.groupType = params['type'];
        }
        if (params.specialty) {
            this.specialty = new specialty_1.Specialty(params.specialty);
        }
    }
    Object.defineProperty(Group.prototype, "id", {
        /**
         *
         * @returns {number}
         */
        get: function () {
            return this._id;
        },
        /**
         *
         * @param value
         */
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "name", {
        /**
         *
         * @returns {string}
         */
        get: function () {
            return this._name;
        },
        /**
         *
         * @param value
         */
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "startYear", {
        /**
         *
         * @returns {number}
         */
        get: function () {
            return this._startYear;
        },
        /**
         *
         * @param value
         */
        set: function (value) {
            this._startYear = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "specialty", {
        /**
         *
         * @returns {Specialty}
         */
        get: function () {
            return this._specialty;
        },
        /**
         *
         * @param value
         */
        set: function (value) {
            this._specialty = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Group.prototype, "groupType", {
        /**
         *
         * @returns {any}
         */
        get: function () {
            return this._groupType;
        },
        /**
         *
         * @param value
         */
        set: function (value) {
            this._groupType = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     *
     * @returns {{id: number, name: string, startYear: number, specialtyId: number, groupType: any}}
     */
    Group.prototype.getJsonForServer = function () {
        var self = this;
        var data = {
            id: self.id,
            name: self.name,
            startYear: self.startYear,
            specialtyId: self.specialty.id,
            groupType: self.groupType
        };
        return data;
    };
    /**
     *
     * @returns {Group}
     */
    Group.prototype.clear = function () {
        this.id = null;
        this.name = null;
        this.startYear = null;
        this.specialty = new specialty_1.Specialty();
        this.groupType = null;
        return this;
    };
    return Group;
}());
exports.Group = Group;
//# sourceMappingURL=group.js.map