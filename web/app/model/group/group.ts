import {GroupInterface} from "./group.interface";
import {Specialty} from "../specialty/specialty";

export class Group implements GroupInterface {
    private _id: number;
    private _name: string;
    private _startYear: number;
    private _specialty: Specialty = new Specialty();
    private _groupType: any;

    /**
     *
     * @param params
     */
    constructor(params?: GroupInterface) {
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.startYear) {
            this.startYear = params.startYear;
        }
        if (params.hasOwnProperty('start_year')) {
            this.startYear = params['start_year'];
        }
        if (params.groupType) {
            this.groupType = params.groupType;
        }
        if (params.hasOwnProperty('type')) {
            this.groupType = params['type'];
        }
        if (params.specialty) {
            this.specialty = new Specialty(params.specialty);
        }
    }

    /**
     *
     * @returns {number}
     */
    get id(): number {
        return this._id;
    }

    /**
     *
     * @param value
     */
    set id(value: number) {
        this._id = value;
    }

    /**
     *
     * @returns {string}
     */
    get name(): string {
        return this._name;
    }

    /**
     *
     * @param value
     */
    set name(value: string) {
        this._name = value;
    }

    /**
     *
     * @returns {number}
     */
    get startYear(): number {
        return this._startYear;
    }

    /**
     *
     * @param value
     */
    set startYear(value: number) {
        this._startYear = value;
    }

    /**
     *
     * @returns {Specialty}
     */
    get specialty(): Specialty {
        return this._specialty;
    }

    /**
     *
     * @param value
     */
    set specialty(value: Specialty) {
        this._specialty = value;
    }

    /**
     *
     * @returns {any}
     */
    get groupType(): any {
        return this._groupType;
    }

    /**
     *
     * @param value
     */
    set groupType(value: any) {
        this._groupType = value;
    }

    /**
     *
     * @returns {{id: number, name: string, startYear: number, specialtyId: number, groupType: any}}
     */
    getJsonForServer() {
        let self = this;
        let data = {
            id: self.id,
            name: self.name,
            startYear: self.startYear,
            specialtyId: self.specialty.id,
            groupType: self.groupType
        };
        return data;
    }

    /**
     *
     * @returns {Group}
     */
    clear() {
        this.id = null;
        this.name = null;
        this.startYear = null;
        this.specialty = new Specialty();
        this.groupType = null;
        return this;
    }
}