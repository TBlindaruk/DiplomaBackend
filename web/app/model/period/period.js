"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var student_1 = require("../student/student");
var group_1 = require("../group/group");
var Period = (function () {
    function Period(params) {
        this._students = [];
        /**@TODO: Change*/
        this.facultyId = null;
        this.params = null;
        var self = this;
        this.params = params;
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.student_period_groups) {
            for (var i = 0; i < params.student_period_groups.length; ++i) {
                var student = new student_1.Student(params.student_period_groups[i].student);
                student.group = new group_1.Group(params.student_period_groups[i].group);
                this.students.push(student);
            }
        }
        self.sheetsDbId = self.getDSheetsId(params);
        self.sheetsRatingId = self.getDBRatingId(params);
        self.scholarship = self.getScholarship(params);
    }
    Object.defineProperty(Period.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Period.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Period.prototype, "sheetsDbId", {
        get: function () {
            return this._sheetsDbId;
        },
        set: function (value) {
            this._sheetsDbId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Period.prototype, "sheetsRatingId", {
        get: function () {
            return this._sheetsRatingId;
        },
        set: function (value) {
            this._sheetsRatingId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Period.prototype, "scholarship", {
        get: function () {
            return this._scholarship;
        },
        set: function (value) {
            this._scholarship = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Period.prototype, "students", {
        get: function () {
            return this._students;
        },
        set: function (value) {
            this._students = value;
        },
        enumerable: true,
        configurable: true
    });
    Period.prototype.addStudent = function (student) {
        this._students.push(student);
    };
    Period.prototype.getDSheetsId = function (response) {
        var self = this, facultyPeriods = response['faculty_period'], result = null;
        for (var _i = 0, facultyPeriods_1 = facultyPeriods; _i < facultyPeriods_1.length; _i++) {
            var facultyPeriod = facultyPeriods_1[_i];
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.db_sheets_id;
                break;
            }
        }
        return result;
    };
    Period.prototype.getScholarship = function (response) {
        var self = this, facultyPeriods = response['faculty_period'], result = null;
        for (var _i = 0, facultyPeriods_2 = facultyPeriods; _i < facultyPeriods_2.length; _i++) {
            var facultyPeriod = facultyPeriods_2[_i];
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.scholarship;
                break;
            }
        }
        return result;
    };
    Period.prototype.getDBRatingId = function (response) {
        var self = this, facultyPeriods = response['faculty_period'], result = null;
        for (var _i = 0, facultyPeriods_3 = facultyPeriods; _i < facultyPeriods_3.length; _i++) {
            var facultyPeriod = facultyPeriods_3[_i];
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.rating_subject_lists;
                if (facultyPeriod.result_rating_subject_lists) {
                    result = facultyPeriod.result_rating_subject_lists;
                }
                if (result) {
                    result = JSON.parse(result);
                }
                else {
                    result = null;
                }
                break;
            }
        }
        return result;
    };
    Period.prototype.getSheetsRatingId = function (facultyId) {
        this.facultyId = facultyId;
        return this.getDBRatingId(this.params);
    };
    return Period;
}());
exports.Period = Period;
//# sourceMappingURL=period.js.map