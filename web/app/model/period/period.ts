import {Student} from "../student/student";
import {Group} from "../group/group";

export interface IPeriod {
    id?: number;
    name: string;
}

export class Period implements IPeriod {

    private _id: number;
    private _name: string;
    private _sheetsDbId: string;
    private _sheetsRatingId: any;
    private _scholarship: string;
    private _students: Student[] = [];



    /**@TODO: Change*/
    public facultyId:number|string = null;
    public params:any = null;

    constructor(params?: any) {
        let self = this;
        this.params = params;
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.student_period_groups) {
            for (let i = 0; i < params.student_period_groups.length; ++i) {
                let student = new Student(params.student_period_groups[i].student);
                student.group = new Group(params.student_period_groups[i].group);
                this.students.push(student);
            }
        }

        self.sheetsDbId = self.getDSheetsId(params);
        self.sheetsRatingId = self.getDBRatingId(params);
        self.scholarship = self.getScholarship(params);


    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get sheetsDbId(): string {
        return this._sheetsDbId;
    }

    set sheetsDbId(value: string) {
        this._sheetsDbId = value;
    }

    get sheetsRatingId(): any {
        return this._sheetsRatingId;
    }

    set sheetsRatingId(value: any) {
        this._sheetsRatingId = value;
    }

    get scholarship(): string {
        return this._scholarship;
    }

    set scholarship(value: string) {
        this._scholarship = value;
    }

    get students(): Student[] {
        return this._students;
    }

    set students(value: Student[]) {
        this._students = value;
    }

    public addStudent(student: Student) {
        this._students.push(student);
    }


    protected getDSheetsId(response: any) {
        let self = this,
            facultyPeriods = response['faculty_period'],
            result: any = null;

        for (let facultyPeriod of facultyPeriods) {
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.db_sheets_id;
                break;
            }
        }
        return result;
    }

    protected getScholarship(response: any){
        let self = this,
            facultyPeriods = response['faculty_period'],
            result: any = null;

        for (let facultyPeriod of facultyPeriods) {
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.scholarship;
                break;
            }
        }
        return result;
    }

    protected getDBRatingId(response: any) {
        let self = this,
            facultyPeriods = response['faculty_period'],
            result: any = null;

        for (let facultyPeriod of facultyPeriods) {
            if (facultyPeriod.faculty.id === self.facultyId) {
                result = facultyPeriod.rating_subject_lists;
                if(facultyPeriod.result_rating_subject_lists){
                    result = facultyPeriod.result_rating_subject_lists;
                }
                if (result) {
                    result = JSON.parse(result);
                } else {
                    result = null;
                }
                break;
            }
        }
        return result;
    }

    getSheetsRatingId(facultyId:number){
        this.facultyId = facultyId;
       return this.getDBRatingId(this.params);
    }
}

