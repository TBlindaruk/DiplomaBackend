import {Faculty} from "../faculty/faculty";
export interface SpecialtyInterface{
    id: number;
    name: string;
    faculty?: Faculty;
}