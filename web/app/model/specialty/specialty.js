"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var faculty_1 = require("../faculty/faculty");
var Specialty = (function () {
    function Specialty(params) {
        this.faculty = new faculty_1.Faculty();
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.faculty) {
            if (params.faculty instanceof faculty_1.Faculty) {
                this.faculty = params.faculty;
            }
            else {
                this.faculty = new faculty_1.Faculty(params.faculty);
            }
        }
    }
    Object.defineProperty(Specialty.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Specialty.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Specialty.prototype, "faculty", {
        get: function () {
            return this._faculty;
        },
        set: function (value) {
            this._faculty = value;
        },
        enumerable: true,
        configurable: true
    });
    Specialty.prototype.clear = function () {
        this.id = null;
        this.name = null;
        this.faculty = new faculty_1.Faculty();
        return this;
    };
    Specialty.prototype.getJsonForServer = function () {
        var self = this;
        return {
            id: self.id,
            name: self.name,
            facultyId: self.faculty.id
        };
    };
    return Specialty;
}());
exports.Specialty = Specialty;
//# sourceMappingURL=specialty.js.map