import {SpecialtyInterface} from "./specialty.interface";
import {Faculty} from "../faculty/faculty";
import {FacultyInterface} from "../faculty/faculty.interface";

export class Specialty implements SpecialtyInterface{
    private _id: number;
    private _name: string;
    private _faculty: Faculty;

    constructor(params?: SpecialtyInterface) {
        this.faculty = new Faculty();

        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.faculty) {
            if(params.faculty instanceof Faculty){
                this.faculty = params.faculty;
            }
            else{
                this.faculty = new Faculty(params.faculty);
            }
        }
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get faculty(): Faculty {
        return this._faculty;
    }

    set faculty(value: Faculty) {
        this._faculty = value;
    }

    clear(): Specialty {
        this.id = null;
        this.name = null;
        this.faculty = new Faculty();
        return this;
    }

    getJsonForServer(){
        let self = this;
        return{
            id: self.id,
            name: self.name,
            facultyId: self.faculty.id
        }
    }
}