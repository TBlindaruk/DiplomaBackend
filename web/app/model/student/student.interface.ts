import {GroupInterface} from "../group/group.interface";
export interface StudentInterface {
    id: number;
    name: string;
    faculty?: GroupInterface;
}