"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var group_1 = require("../group/group");
var period_1 = require("../period/period");
var Student = (function () {
    function Student(params) {
        this._group = new group_1.Group();
        this._period = new period_1.Period();
        this._isRoot = false;
        this._children = [];
        this._isShow = true;
        this._rating = null;
        this._img = null;
        var self = this;
        if (!params) {
            return;
        }
        if (params.id) {
            this.id = params.id;
        }
        if (params.name) {
            this.name = params.name;
        }
        if (params.group) {
            this.group = new group_1.Group(params.group);
        }
        if (params.student_period_groups && params.student_period_groups[0] && params.student_period_groups[0].group) {
            this.group = new group_1.Group(params.student_period_groups[0].group);
        }
        if (params.period) {
            this.period = new period_1.Period(params.period);
        }
        if (params.student_period_groups && params.student_period_groups[0] && params.student_period_groups[0].period) {
            this.period = new period_1.Period(params.student_period_groups[0].period);
        }
        if (params.is_root || params.isRoot) {
            this.isRoot = true;
        }
        if (params.children) {
            for (var i = 0; i < params.children.length; ++i) {
                self.addChildren(new Student(params.children[i]));
            }
        }
        if (params.parent) {
            this.parent = new Student(params.parent);
        }
        if (params.rating) {
            this.rating = params.rating;
        }
        if (params.src) {
            this.img = params.src;
        }
    }
    Object.defineProperty(Student.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "group", {
        get: function () {
            return this._group;
        },
        set: function (value) {
            this._group = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "period", {
        get: function () {
            return this._period;
        },
        set: function (value) {
            this._period = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "isRoot", {
        get: function () {
            return this._isRoot;
        },
        set: function (value) {
            this._isRoot = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "parent", {
        get: function () {
            if (!this._parent) {
                this._parent = new Student();
            }
            return this._parent;
        },
        set: function (value) {
            this._parent = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "children", {
        get: function () {
            return this._children;
        },
        set: function (value) {
            this._children = value;
        },
        enumerable: true,
        configurable: true
    });
    Student.prototype.addChildren = function (student) {
        student.parent = this;
        this._children.push(student);
    };
    Object.defineProperty(Student.prototype, "isShow", {
        get: function () {
            return this._isShow;
        },
        set: function (value) {
            this._isShow = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "rating", {
        get: function () {
            return this._rating;
        },
        set: function (value) {
            this._rating = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "img", {
        get: function () {
            return this._img;
        },
        set: function (value) {
            this._img = value;
        },
        enumerable: true,
        configurable: true
    });
    Student.prototype.getJsonForServer = function () {
        var self = this;
        return {
            id: self.id,
            name: self.name,
            groupId: self.group.id,
            periodId: self.period.id,
            isRoot: self.isRoot,
            parentId: self.parent.id,
            rating: self.rating,
            img: self.img
        };
    };
    Student.prototype.clear = function () {
        this.id = null;
        this.name = null;
        this.group = new group_1.Group();
        this.period = new period_1.Period();
        this.isRoot = false;
        this.parent = new Student();
        this.isShow = true;
        this.rating = null;
        this.img = null;
    };
    return Student;
}());
exports.Student = Student;
//# sourceMappingURL=student.js.map