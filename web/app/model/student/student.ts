import {StudentInterface} from "./student.interface";
import {Group} from "../group/group";
import {Period} from "../period/period";
export class Student implements StudentInterface {
    private _id: number;
    private _name: string;
    private _group: Group = new Group();
    private _period: Period = new Period();
    private _isRoot: boolean = false;
    private _parent: Student;
    private _children: Student[] = [];
    private _isShow: boolean = true;
    private _rating: number = null;
    private _img: string = null;


    constructor(params?: any) {
        let self = this;
        if (!params) {
            return
        }

        if (params.id) {
            this.id = params.id;
        }

        if (params.name) {
            this.name = params.name;
        }

        if (params.group) {
            this.group = new Group(params.group);
        }
        if (params.student_period_groups && params.student_period_groups[0] && params.student_period_groups[0].group) {
            this.group = new Group(params.student_period_groups[0].group);
        }

        if (params.period) {
            this.period = new Period(params.period);
        }
        if (params.student_period_groups && params.student_period_groups[0] && params.student_period_groups[0].period) {
            this.period = new Period(params.student_period_groups[0].period);
        }

        if (params.is_root || params.isRoot) {
            this.isRoot = true;
        }

        if (params.children) {
            for (let i = 0; i < params.children.length; ++i) {
                self.addChildren(new Student(params.children[i]));
            }
        }

        if (params.parent) {
            this.parent = new Student(params.parent);
        }

        if (params.rating) {
            this.rating = params.rating;
        }

        if (params.src) {
            this.img = params.src;
        }
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get group(): Group {
        return this._group;
    }

    set group(value: Group) {
        this._group = value;
    }

    get period(): Period {
        return this._period;
    }

    set period(value: Period) {
        this._period = value;
    }

    get isRoot(): boolean {
        return this._isRoot;
    }

    set isRoot(value: boolean) {
        this._isRoot = value;
    }

    get parent(): Student {
        if (!this._parent) {
            this._parent = new Student();
        }
        return this._parent;
    }

    set parent(value: Student) {
        this._parent = value;
    }

    get children(): Student[] {
        return this._children;
    }

    set children(value: Student[]) {
        this._children = value;
    }

    public addChildren(student: Student) {
        student.parent = this;
        this._children.push(student);
    }

    get isShow(): boolean {
        return this._isShow;
    }

    set isShow(value: boolean) {
        this._isShow = value;
    }

    get rating(): number {
        return this._rating;
    }

    set rating(value: number) {
        this._rating = value;
    }

    get img(): any {
        return this._img;
    }

    set img(value: any) {
        this._img = value;
    }


    getJsonForServer() {
        let self = this;
        return {
            id: self.id,
            name: self.name,
            groupId: self.group.id,
            periodId: self.period.id,
            isRoot: self.isRoot,
            parentId: self.parent.id,
            rating: self.rating,
            img: self.img
        }
    }


    clear() {
        this.id = null;
        this.name = null;
        this.group = new Group();
        this.period = new Period();
        this.isRoot = false;
        this.parent = new Student();
        this.isShow = true;
        this.rating = null;
        this.img = null;
    }
}