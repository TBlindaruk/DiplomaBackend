"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var http_1 = require("@angular/http");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
var faculty_settings_1 = require("../../settings/faculty.settings");
var FacultyService = (function () {
    function FacultyService(_http) {
        this._http = _http;
        this._facultyListUrl = faculty_settings_1.FacultySettings.API_FACULTY_LIST;
        this._facultyCreateUrl = faculty_settings_1.FacultySettings.API_FACULTY_CREATE;
        this._facultyEditUrl = faculty_settings_1.FacultySettings.API_FACULTY_EDIT;
        this._facultyDeleteUrl = faculty_settings_1.FacultySettings.API_FACULTY_DELETE;
        this._facultyGetUrl = faculty_settings_1.FacultySettings.API_FACULTY_GET;
    }
    FacultyService.prototype.getFaculties = function () {
        return this._http.get(this._facultyListUrl)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    FacultyService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    FacultyService.prototype.addFaculty = function (period) {
        return this._http.post(this._facultyCreateUrl, period)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    FacultyService.prototype.editFaculty = function (faculty) {
        return this._http.put(this._facultyEditUrl(faculty.id), faculty)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    FacultyService.prototype.deleteFaculty = function (faculty) {
        var data = {};
        return this._http.delete(this._facultyDeleteUrl(faculty.id), data)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    FacultyService.prototype.getFaculty = function (id) {
        return this._http.get(this._facultyGetUrl(id))
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    return FacultyService;
}());
FacultyService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], FacultyService);
exports.FacultyService = FacultyService;
//# sourceMappingURL=faculty.service.js.map