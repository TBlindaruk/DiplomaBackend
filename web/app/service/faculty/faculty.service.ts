import {Injectable} from "@angular/core";
import { Observable } from 'rxjs/Observable';
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {FacultySettings} from "../../settings/faculty.settings";
import {Faculty} from "../../model/faculty/faculty";



@Injectable()
export class FacultyService{

    private _facultyListUrl = FacultySettings.API_FACULTY_LIST;
    private _facultyCreateUrl = FacultySettings.API_FACULTY_CREATE;
    private _facultyEditUrl = FacultySettings.API_FACULTY_EDIT;
    private _facultyDeleteUrl = FacultySettings.API_FACULTY_DELETE;
    private _facultyGetUrl = FacultySettings.API_FACULTY_GET;

    constructor(private _http: Http) {}

    public getFaculties(): Observable<Faculty[]> {
        return this._http.get(this._facultyListUrl)
            .map((response: Response) => <Faculty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    public addFaculty(period:Faculty): Observable<Faculty>{
        return this._http.post(this._facultyCreateUrl,period)
            .map((response: Response) => <Faculty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    public editFaculty(faculty:Faculty): Observable<Faculty>{
        return this._http.put(this._facultyEditUrl(faculty.id),faculty)
            .map((response: Response) => <Faculty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    public deleteFaculty(faculty:Faculty): Observable<Faculty>{
        let data = {
        };
        return this._http.delete(this._facultyDeleteUrl(faculty.id),data)
            .map((response: Response) => <Faculty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    public getFaculty(id:number): Observable<any>{
        return this._http.get(this._facultyGetUrl(id))
            .map((response: Response) => <Faculty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }
}