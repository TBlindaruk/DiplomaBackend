"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var http_1 = require("@angular/http");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
var group_settings_1 = require("../../settings/group.settings");
var GroupService = (function () {
    function GroupService(_http) {
        this._http = _http;
        this._groupListUrl = group_settings_1.GroupSettings.API_GROUP_LIST;
        this._groupCreateUrl = group_settings_1.GroupSettings.API_GROUP_CREATE;
        this._groupEditUrl = group_settings_1.GroupSettings.API_GROUP_EDIT;
        this._groupDeleteUrl = group_settings_1.GroupSettings.API_GROUP_DELETE;
    }
    GroupService.prototype.getGroups = function () {
        return this._http.get(this._groupListUrl)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    GroupService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    GroupService.prototype.addGroup = function (period) {
        return this._http.post(this._groupCreateUrl, period.getJsonForServer())
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    GroupService.prototype.editGroup = function (group) {
        return this._http.put(this._groupEditUrl(group.id), group.getJsonForServer())
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    GroupService.prototype.deleteGroup = function (faculty) {
        var data = {};
        return this._http.delete(this._groupDeleteUrl(faculty.id), data)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    return GroupService;
}());
GroupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], GroupService);
exports.GroupService = GroupService;
//# sourceMappingURL=group.service.js.map