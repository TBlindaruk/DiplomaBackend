import {Injectable} from "@angular/core";
import {Observable} from 'rxjs/Observable';
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {GroupSettings} from "../../settings/group.settings";
import {Group} from "../../model/group/group";


@Injectable()
export class GroupService {

    private _groupListUrl = GroupSettings.API_GROUP_LIST;
    private _groupCreateUrl = GroupSettings.API_GROUP_CREATE;
    private _groupEditUrl = GroupSettings.API_GROUP_EDIT;
    private _groupDeleteUrl = GroupSettings.API_GROUP_DELETE;

    constructor(private _http: Http) {
    }

    public getGroups(): Observable<Group[]> {
        return this._http.get(this._groupListUrl)
            .map((response: Response) => <Group[]> response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    public addGroup(period: Group): Observable<Group> {
        return this._http.post(this._groupCreateUrl, period.getJsonForServer())
            .map((response: Response) => <Group[]> response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }


    public editGroup(group: Group): Observable<Group> {
        return this._http.put(this._groupEditUrl(group.id), group.getJsonForServer())
            .map((response: Response) => <Group[]> response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    public deleteGroup(faculty: Group): Observable<Group> {
        let data = {};
        return this._http.delete(this._groupDeleteUrl(faculty.id), data)
            .map((response: Response) => <Group[]> response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
}