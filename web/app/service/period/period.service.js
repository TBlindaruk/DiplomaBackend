"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
var period_settings_1 = require("../../settings/period.settings");
var PeriodService = (function () {
    function PeriodService(_http) {
        this._http = _http;
        this._periodListUrl = period_settings_1.PeriodSettings.API_PERIOD_LIST;
        this._periodCreateUrl = period_settings_1.PeriodSettings.API_PERIOD_CREATE;
        this._periodGetUrl = period_settings_1.PeriodSettings.API_PERIOD_GET;
    }
    PeriodService.prototype.getPeriods = function () {
        return this._http.get(this._periodListUrl)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    PeriodService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    PeriodService.prototype.addPeriod = function (period) {
        return this._http.post(this._periodCreateUrl, period)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    PeriodService.prototype.getPeriod = function (id) {
        return this._http.get(this._periodGetUrl + "/" + id)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    PeriodService.prototype.createGoogleSheets = function (requestUrl) {
        return this._http.post(requestUrl, {})
            .map(function (response) {
            var data = null;
            try {
                data = response.json();
            }
            catch (e) {
            }
            return data;
        })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    return PeriodService;
}());
PeriodService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PeriodService);
exports.PeriodService = PeriodService;
//# sourceMappingURL=period.service.js.map