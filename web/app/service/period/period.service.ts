import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {PeriodSettings} from "../../settings/period.settings";
import {Period} from "../../model/period/period";


@Injectable()
export class PeriodService {
    private _periodListUrl = PeriodSettings.API_PERIOD_LIST;
    private _periodCreateUrl = PeriodSettings.API_PERIOD_CREATE;
    private _periodGetUrl = PeriodSettings.API_PERIOD_GET;

    constructor(private _http: Http) {
    }

    public getPeriods(): Observable<Period[]> {
        return this._http.get(this._periodListUrl)
            .map((response: Response) => <Period[]> response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    public addPeriod(period: Period): Observable<Period> {
        return this._http.post(this._periodCreateUrl, period)
            .map((response: Response) => <Period[]> response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    public getPeriod(id: any): Observable<any> {
        return this._http.get(`${this._periodGetUrl}/${id}`)
            .map((response: Response) => <Period[]> response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    public createGoogleSheets(requestUrl: string): Observable<any> {
        return this._http.post(requestUrl, {})
            .map(function(response: Response){
                let data = null;
                try {
                    data = response.json();
                }catch (e){

                }
               return data;
            })
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
}
