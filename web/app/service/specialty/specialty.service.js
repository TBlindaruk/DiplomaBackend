"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var specialty_settings_1 = require("../../settings/specialty.settings");
var SpecialityService = (function () {
    function SpecialityService(_http) {
        this._http = _http;
        this._specialtyListUrl = specialty_settings_1.SpecialtySettings.API_SPECIALTY_LIST;
        this._specialtyCreateUrl = specialty_settings_1.SpecialtySettings.API_SPECIALTY_CREATE;
        this._specialtyEditUrl = specialty_settings_1.SpecialtySettings.API_SPECIALTY_EDIT;
        this._specialtyDeleteUrl = specialty_settings_1.SpecialtySettings.API_SPECIALTY_DELETE;
    }
    SpecialityService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    SpecialityService.prototype.getSpecialties = function () {
        return this._http.get(this._specialtyListUrl)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    SpecialityService.prototype.addSpecialty = function (specialty) {
        return this._http.post(this._specialtyCreateUrl, specialty.getJsonForServer())
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    SpecialityService.prototype.editSpecialty = function (specialty) {
        return this._http.put(this._specialtyEditUrl(specialty.id), specialty.getJsonForServer())
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    SpecialityService.prototype.deleteSpecialty = function (specialty) {
        return this._http.delete(this._specialtyDeleteUrl(specialty.id), {})
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    return SpecialityService;
}());
SpecialityService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], SpecialityService);
exports.SpecialityService = SpecialityService;
//# sourceMappingURL=specialty.service.js.map