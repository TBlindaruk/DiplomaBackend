import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Specialty} from "../../model/specialty/specialty";
import {Observable} from "rxjs/Observable";
import {SpecialtySettings} from "../../settings/specialty.settings";
@Injectable()
export class SpecialityService{
    private _specialtyListUrl = SpecialtySettings.API_SPECIALTY_LIST;
    private _specialtyCreateUrl = SpecialtySettings.API_SPECIALTY_CREATE;
    private _specialtyEditUrl = SpecialtySettings.API_SPECIALTY_EDIT;
    private _specialtyDeleteUrl = SpecialtySettings.API_SPECIALTY_DELETE;

    constructor(private _http: Http){}

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    public getSpecialties(): Observable<Specialty[]>{
        return this._http.get(this._specialtyListUrl)
                .map((response: Response) => <Specialty[]> response.json())
                .do(data => console.log('All: ' +  JSON.stringify(data)))
                .catch(this.handleError);
    }

    public addSpecialty(specialty:Specialty): Observable<Specialty>{
        return this._http.post(this._specialtyCreateUrl,specialty.getJsonForServer())
            .map((response: Response) => <Specialty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    public editSpecialty(specialty:Specialty): Observable<Specialty>{
        return this._http.put(this._specialtyEditUrl(specialty.id),specialty.getJsonForServer())
            .map((response: Response) => <Specialty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }


    public deleteSpecialty(specialty:Specialty): Observable<Specialty>{
        return this._http.delete(this._specialtyDeleteUrl(specialty.id),{})
            .map((response: Response) => <Specialty[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }
}