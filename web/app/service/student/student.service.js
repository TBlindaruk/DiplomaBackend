"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var student_settings_1 = require("../../settings/student.settings");
var StudentService = (function () {
    function StudentService(_http) {
        this._http = _http;
        this._studentListUrl = student_settings_1.StudentSettings.API_STUDENT_LIST;
        this._studentCreateUrl = student_settings_1.StudentSettings.API_STUDENT_CREATE;
        this._studentEditUrl = student_settings_1.StudentSettings.API_STUDENT_EDIT;
        this._studentDeleteUrl = student_settings_1.StudentSettings.API_STUDENT_DELETE;
    }
    StudentService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    StudentService.prototype.getStudents = function () {
        return this._http.get(this._studentListUrl)
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    StudentService.prototype.addStudent = function (specialty) {
        return this._http.post(this._studentCreateUrl, specialty.getJsonForServer())
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    StudentService.prototype.editStudent = function (specialty) {
        return this._http.put(this._studentEditUrl(specialty.id), specialty.getJsonForServer())
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    StudentService.prototype.deleteStudent = function (specialty) {
        return this._http.delete(this._studentDeleteUrl(specialty.id), {})
            .map(function (response) { return response.json(); })
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    return StudentService;
}());
StudentService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], StudentService);
exports.StudentService = StudentService;
//# sourceMappingURL=student.service.js.map