import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Student} from "../../model/student/student";
import {StudentSettings} from '../../settings/student.settings';

@Injectable()
export class StudentService{
    private _studentListUrl = StudentSettings.API_STUDENT_LIST;
    private _studentCreateUrl = StudentSettings.API_STUDENT_CREATE;
    private _studentEditUrl = StudentSettings.API_STUDENT_EDIT;
    private _studentDeleteUrl = StudentSettings.API_STUDENT_DELETE;

    constructor(private _http: Http){}

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

    public getStudents(): Observable<Student[]>{
        return this._http.get(this._studentListUrl)
            .map((response: Response) => <Student[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    public addStudent(specialty:Student): Observable<Student>{
        return this._http.post(this._studentCreateUrl,specialty.getJsonForServer())
            .map((response: Response) => <Student[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    public editStudent(specialty:Student): Observable<Student>{
        return this._http.put(this._studentEditUrl(specialty.id),specialty.getJsonForServer())
            .map((response: Response) => <Student[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    public deleteStudent(specialty:Student): Observable<Student>{
        return this._http.delete(this._studentDeleteUrl(specialty.id),{})
            .map((response: Response) => <Student[]> response.json())
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }
}