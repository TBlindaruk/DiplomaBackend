"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_settings_1 = require("../app.settings");
var FacultySettings = (function () {
    function FacultySettings() {
    }
    return FacultySettings;
}());
FacultySettings.API_FACULTY_LIST = app_settings_1.AppSettings.API_ENDPOINT + 'faculty';
FacultySettings.API_FACULTY_CREATE = app_settings_1.AppSettings.API_ENDPOINT + 'faculty';
FacultySettings.API_FACULTY_EDIT = function (id) {
    return app_settings_1.AppSettings.API_ENDPOINT + ("faculty/" + id);
};
FacultySettings.API_FACULTY_DELETE = function (id) {
    return app_settings_1.AppSettings.API_ENDPOINT + ("faculty/" + id);
};
FacultySettings.API_FACULTY_GET = function (id) {
    return app_settings_1.AppSettings.API_ENDPOINT + ("faculty/" + id);
};
exports.FacultySettings = FacultySettings;
//# sourceMappingURL=faculty.settings.js.map