import {AppSettings} from "../app.settings";
export class FacultySettings {
    public static API_FACULTY_LIST= AppSettings.API_ENDPOINT + 'faculty';
    public static API_FACULTY_CREATE= AppSettings.API_ENDPOINT + 'faculty';
    public static API_FACULTY_EDIT= function(id:number){
        return AppSettings.API_ENDPOINT + `faculty/${id}` ;
    };
    public static API_FACULTY_DELETE= function(id:number){
        return AppSettings.API_ENDPOINT + `faculty/${id}` ;
    };
    public static API_FACULTY_GET= function(id:number){
        return AppSettings.API_ENDPOINT + `faculty/${id}` ;
    };

}