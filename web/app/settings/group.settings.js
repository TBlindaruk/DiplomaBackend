"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_settings_1 = require("../app.settings");
var GroupSettings = (function () {
    function GroupSettings() {
    }
    return GroupSettings;
}());
GroupSettings.API_GROUP_LIST = app_settings_1.AppSettings.API_ENDPOINT + 'group';
GroupSettings.API_GROUP_CREATE = app_settings_1.AppSettings.API_ENDPOINT + 'group';
GroupSettings.API_GROUP_EDIT = function (id) {
    return app_settings_1.AppSettings.API_ENDPOINT + ("group/" + id);
};
GroupSettings.API_GROUP_DELETE = function (id) {
    return app_settings_1.AppSettings.API_ENDPOINT + ("group/" + id);
};
exports.GroupSettings = GroupSettings;
//# sourceMappingURL=group.settings.js.map