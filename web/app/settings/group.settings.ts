import {AppSettings} from "../app.settings";
export class GroupSettings {
    public static API_GROUP_LIST = AppSettings.API_ENDPOINT + 'group';
    public static API_GROUP_CREATE = AppSettings.API_ENDPOINT + 'group';
    public static API_GROUP_EDIT = function (id: number) {
        return AppSettings.API_ENDPOINT + `group/${id}`;
    };
    public static API_GROUP_DELETE = function (id: number) {
        return AppSettings.API_ENDPOINT + `group/${id}`;
    };
}