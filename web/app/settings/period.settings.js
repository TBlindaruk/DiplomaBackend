"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_settings_1 = require("../app.settings");
var PeriodSettings = (function () {
    function PeriodSettings() {
    }
    return PeriodSettings;
}());
PeriodSettings.API_PERIOD_LIST = app_settings_1.AppSettings.API_ENDPOINT + 'period';
PeriodSettings.API_PERIOD_CREATE = app_settings_1.AppSettings.API_ENDPOINT + 'period';
PeriodSettings.API_PERIOD_GET = app_settings_1.AppSettings.API_ENDPOINT + 'period';
PeriodSettings.API_PERIOD_FACULTY_CREATE_GOOGLE_SHEETS_DB = function ($periodId, facultyId) {
    return app_settings_1.AppSettings.API_ENDPOINT + "google/period/" + $periodId + "/faculty/" + facultyId + "/db/";
};
PeriodSettings.API_PERIOD_FACULTY_CREATE_RATING_DB = function ($periodId, facultyId) {
    return app_settings_1.AppSettings.API_ENDPOINT + "google/period/" + $periodId + "/faculty/" + facultyId + "/data/";
};
PeriodSettings.API_PERIOD_FACULTY_CREATE_RESULT_RATING_DB = function ($periodId, facultyId) {
    return app_settings_1.AppSettings.API_ENDPOINT + "google/period/" + $periodId + "/faculty/" + facultyId + "/result/";
};
PeriodSettings.API_PERIOD_FACULTY_CREATE_SCHOLARSHIP_DB = function ($periodId, facultyId) {
    return app_settings_1.AppSettings.API_ENDPOINT + "google/period/" + $periodId + "/faculty/" + facultyId + "/grants/";
};
exports.PeriodSettings = PeriodSettings;
//# sourceMappingURL=period.settings.js.map