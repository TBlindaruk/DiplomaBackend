import {AppSettings} from "../app.settings";
export class PeriodSettings {
    public static API_PERIOD_LIST = AppSettings.API_ENDPOINT + 'period';
    public static API_PERIOD_CREATE = AppSettings.API_ENDPOINT + 'period';
    public static API_PERIOD_GET = AppSettings.API_ENDPOINT + 'period';
    public static API_PERIOD_FACULTY_CREATE_GOOGLE_SHEETS_DB = function ($periodId: number | string, facultyId: number | string) {
        return `${AppSettings.API_ENDPOINT}google/period/${$periodId}/faculty/${facultyId}/db/`;
    };
    public static API_PERIOD_FACULTY_CREATE_RATING_DB = function ($periodId: number | string, facultyId: number | string) {
        return `${AppSettings.API_ENDPOINT}google/period/${$periodId}/faculty/${facultyId}/data/`;
    };
    public static API_PERIOD_FACULTY_CREATE_RESULT_RATING_DB = function ($periodId: number | string, facultyId: number | string) {
        return `${AppSettings.API_ENDPOINT}google/period/${$periodId}/faculty/${facultyId}/result/`;
    };
    public static API_PERIOD_FACULTY_CREATE_SCHOLARSHIP_DB = function ($periodId: number | string, facultyId: number | string) {
        return `${AppSettings.API_ENDPOINT}google/period/${$periodId}/faculty/${facultyId}/grants/`;
    };
}