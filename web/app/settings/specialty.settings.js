"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_settings_1 = require("../app.settings");
var SpecialtySettings = (function () {
    function SpecialtySettings() {
    }
    return SpecialtySettings;
}());
SpecialtySettings.API_SPECIALTY_LIST = app_settings_1.AppSettings.API_ENDPOINT + 'specialty';
SpecialtySettings.API_SPECIALTY_CREATE = app_settings_1.AppSettings.API_ENDPOINT + 'specialty';
SpecialtySettings.API_SPECIALTY_EDIT = function (specialtyId) {
    return app_settings_1.AppSettings.API_ENDPOINT + "specialty/" + specialtyId;
};
SpecialtySettings.API_SPECIALTY_DELETE = function (specialtyId) {
    return app_settings_1.AppSettings.API_ENDPOINT + "specialty/" + specialtyId;
};
exports.SpecialtySettings = SpecialtySettings;
//# sourceMappingURL=specialty.settings.js.map