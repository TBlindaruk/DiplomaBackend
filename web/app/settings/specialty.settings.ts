import {AppSettings} from "../app.settings";
export class SpecialtySettings {
    public static API_SPECIALTY_LIST = AppSettings.API_ENDPOINT + 'specialty';
    public static API_SPECIALTY_CREATE = AppSettings.API_ENDPOINT + 'specialty';
    public static API_SPECIALTY_EDIT = function (specialtyId: number | string) {
        return `${AppSettings.API_ENDPOINT}specialty/${specialtyId}`;
    };

    public static API_SPECIALTY_DELETE = function (specialtyId: number | string) {
        return `${AppSettings.API_ENDPOINT}specialty/${specialtyId}`;
    };

}