"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_settings_1 = require("../app.settings");
var StudentSettings = (function () {
    function StudentSettings() {
    }
    return StudentSettings;
}());
StudentSettings.API_STUDENT_LIST = app_settings_1.AppSettings.API_ENDPOINT + 'student';
StudentSettings.API_STUDENT_CREATE = app_settings_1.AppSettings.API_ENDPOINT + 'student';
StudentSettings.API_STUDENT_EDIT = function (id) {
    return app_settings_1.AppSettings.API_ENDPOINT + "student/" + id;
};
StudentSettings.API_STUDENT_DELETE = function (id) {
    return app_settings_1.AppSettings.API_ENDPOINT + "student/" + id;
};
exports.StudentSettings = StudentSettings;
//# sourceMappingURL=student.settings.js.map