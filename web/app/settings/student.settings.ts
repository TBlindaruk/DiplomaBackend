import {AppSettings} from "../app.settings";
export class StudentSettings {
    public static API_STUDENT_LIST = AppSettings.API_ENDPOINT + 'student';
    public static API_STUDENT_CREATE = AppSettings.API_ENDPOINT + 'student';

    public static API_STUDENT_EDIT = function (id: number | string) {
        return `${AppSettings.API_ENDPOINT}student/${id}`;
    };

    public static API_STUDENT_DELETE = function (id: number | string) {
        return `${AppSettings.API_ENDPOINT}student/${id}`;
    };
}